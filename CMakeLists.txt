cmake_minimum_required(VERSION 3.18)

project("moneymaker")

set (${PROJECT_NAME}_SOURCES

#core
moneymaker/core2/core.h
moneymaker/core2/core.cpp

# deposit
moneymaker/core2/deposit/instrument.h
moneymaker/core2/deposit/instrument.cpp
moneymaker/core2/deposit/instrument_enums.h
moneymaker/core2/deposit/portfolio.h
moneymaker/core2/deposit/portfolio.cpp
moneymaker/core2/deposit/position.h
moneymaker/core2/deposit/position.cpp
moneymaker/core2/deposit/transaction.h
moneymaker/core2/deposit/transaction.cpp
moneymaker/core2/deposit/transaction_enums.h

# helper
moneymaker/core2/helper/idmanager.h
moneymaker/core2/helper/idmanager.cpp
moneymaker/core2/helper/sqlconnection.h
moneymaker/core2/helper/sqlconnection.cpp
moneymaker/core2/helper/string_functions.h
moneymaker/core2/helper/string_functions.cpp
moneymaker/core2/helper/container_functions.h
moneymaker/core2/helper/container_functions.cpp
moneymaker/core2/helper/timeint.h

# persistence
moneymaker/core2/persistence/persist_obj.h
moneymaker/core2/persistence/persist_obj.cpp
moneymaker/core2/persistence/deposit/instrument_persist.h
moneymaker/core2/persistence/deposit/instrument_persist.cpp
moneymaker/core2/persistence/deposit/portfolio_persist.h
moneymaker/core2/persistence/deposit/portfolio_persist.cpp
moneymaker/core2/persistence/deposit/position_persist.h
moneymaker/core2/persistence/deposit/position_persist.cpp
moneymaker/core2/persistence/deposit/transaction_persist.h
moneymaker/core2/persistence/deposit/transaction_persist.cpp

)

file (GLOB_RECURSE SRC_DEPOSIT_H
   "moneymaker/core2/deposit/*.h"
)

file (GLOB_RECURSE SRC_DEPOSIT_CPP
   "moneymaker/core2/deposit/*.cpp"
)

file (GLOB_RECURSE SRC_HELPER_H
   "moneymaker/core2/helper/*.h"
)

file (GLOB_RECURSE SRC_HELPER_CPP
   "moneymaker/core2/helper/*.cpp"
)

file (GLOB_RECURSE SRC_PERSIST_H
   "moneymaker/core2/persistence/*.h"
)

file (GLOB_RECURSE SRC_PERSIST_CPP
   "moneymaker/core2/persistence/*.cpp"
)

source_group("deposit" FILES ${SRC_DEPOSIT_H} ${SRC_DEPOSIT_CPP})
source_group("helper" FILES ${SRC_HELPER_H} ${SRC_HELPER_CPP})
source_group("persistence" FILES ${SRC_PERSIST_H} ${SRC_PERSIST_CPP})


set(MYSQLCONNECTOR_DIR "C:/Program Files/MySQL/MySQL Connector C++ 8.0")
find_package(Boost REQUIRED)
find_package(Catch2 REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})
include_directories(${Catch2_INCLUDE_DIRS})

include_directories(${MYSQLCONNECTOR_DIR}/include)
include_directories(${MYSQLCONNECTOR_DIR}/include/jdbc)

include_directories(SYSTEM ${CMAKE_SOURCE_DIR}/moneymaker)

link_directories(${Boost_LIBRARY_DIRS})
link_directories(${MYSQLCONNECTOR_DIR}/lib64/vs14)


message(STATUS ${CMAKE_SOURCE_DIR})
message(STATUS ${Boost_INCLUDE_DIRS})
message(STATUS ${Boost_LIBRARY_DIRS})

if(WIN32)
   add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_SOURCES})
endif(WIN32)

if(UNIX)
   add_library(${PROJECT_NAME} SHARED ${${PROJECT_NAME}_SOURCES})
endif(UNIX)

add_executable(${PROJECT_NAME}_exe moneymaker/main.cpp)

target_link_libraries(${PROJECT_NAME} mysqlcppconn)
target_link_libraries(${PROJECT_NAME}_exe ${PROJECT_NAME})

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

install(TARGETS ${PROJECT_NAME} DESTINATION bin)


# tests

include(CTest)
include(Catch)

add_executable(test_core tests/cpp/test_helper_methods.h tests/cpp/test_core.cpp)
target_link_libraries(test_core ${PROJECT_NAME} Catch2::Catch2)
add_test("test_core" test_core)

add_executable(test_idmanager tests/cpp/test_helper_methods.h tests/cpp/test_idmanager.cpp)
target_link_libraries(test_idmanager ${PROJECT_NAME} Catch2::Catch2)
add_test("test_idmanager" test_idmanager)

add_executable(test_instrument tests/cpp/test_helper_methods.h tests/cpp/test_instrument.cpp)
target_link_libraries(test_instrument ${PROJECT_NAME} Catch2::Catch2)
add_test("test_instrument" test_instrument)

add_executable(test_portfolio tests/cpp/test_helper_methods.h tests/cpp/test_portfolio.cpp)
target_link_libraries(test_portfolio ${PROJECT_NAME} Catch2::Catch2)
add_test("Test_Position" test_portfolio)

add_executable(test_position tests/cpp/test_helper_methods.h tests/cpp/test_position.cpp)
target_link_libraries(test_position ${PROJECT_NAME} Catch2::Catch2)

add_executable(test_sqlcon tests/cpp/test_helper_methods.h tests/cpp/test_sqlcon.cpp)
target_link_libraries(test_sqlcon ${PROJECT_NAME} Catch2::Catch2)
add_test("test_sqlcon" test_sqlcon)

add_executable(test_transaction tests/cpp/test_helper_methods.h tests/cpp/test_transaction.cpp)
target_link_libraries(test_transaction ${PROJECT_NAME} Catch2::Catch2)
add_test("test_transaction" test_transaction)