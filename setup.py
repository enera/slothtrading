import setuptools
import distutils.core
from Cython.Build import cythonize
import numpy


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="moneymaker",
    version="0.0.1",
    author="Pascal.Trader",
    author_email="doenerxl@mail.com",
    description="Makes some money.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    package_dir={},
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    ext_modules=cythonize(
        ["moneymaker/**/*.pyx", "tests/**/*.pyx"],
        compiler_directives={"language_level": "3"},
        language="c++",
    ),
    include_dirs=[numpy.get_include()],
)

# setup command: python setup.py build_ext --inplace
