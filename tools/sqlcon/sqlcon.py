from datetime import datetime
import os
from turtle import back
import uuid
from shutil import copyfile
from pathlib import Path
import sys
import glob


def sqlcon(basepath: str, filename: str, backup_path: str):

    filename_pyx = basepath + filename + ".pyx"
    filename_pxd = basepath + filename + ".pxd"
    tempfile_pyx = basepath + "tempfile.pyx"

    # make a backup of file in case script
    rand_filename = str(uuid.uuid4()).split("-")[0]
    filename_pyx_backup = f"{backup_path}/{filename}_{rand_filename}.pyx"
    Path("sqlcon_backup").mkdir(parents=True, exist_ok=True)
    copyfile(filename_pyx, filename_pyx_backup)

    file1 = open(filename_pxd, "r")
    lines = file1.readlines()
    file1.close()

    default_strlen = 24

    # scan for enum definitions
    comment_section = False
    enums = []
    sql_rules = []
    classname = ""
    build_sqlcon = False
    sqltablename = ""

    for line in lines:

        if line.lstrip().startswith("# build sqlcon"):
            build_sqlcon = True
            continue

        if line.lstrip().startswith("cdef class"):
            s = line.split(" ")
            if len(s) > 2:
                classname = s[2].strip()
                # inherited classes
                if "(" in classname:
                    classname = classname.split("(")[0].strip()
        if line.lstrip().startswith("#") and line.replace("#", "").lstrip().startswith(
            "sqlcon:"
        ):
            s = line.split("sqlcon:")

            if len(s) > 1:
                if "dbtablename" in s[1]:
                    sqltablename = s[1].replace("dbtablename", "").strip()
                    continue

                sql_rules.append(s[1].strip())

        if line.lstrip().startswith("'''") or line.lstrip().startswith('"""'):
            if comment_section is True:
                comment_section = False
                continue
            else:
                comment_section = True

        if line.lstrip().startswith("#") or comment_section is True:
            continue

        if "def enum" in line:
            s = line.split("enum")
            if len(s) > 1:
                enums.append(s[1].replace(":", "").strip())

    if build_sqlcon is False:
        return

    print(
        "Writing methods in file", filename_pyx, "with name of sql table", sqltablename
    )
    # filter lines
    starting_point_class = False
    starting_point_vars = False
    ending_point_vars = False
    comment_section = False

    vars_type = []
    vars_name = []

    for line in lines:
        if line.lstrip().startswith("'''") or line.lstrip().startswith('"""'):
            if comment_section is True:
                comment_section = False
                continue
            else:
                comment_section = True

        if line.lstrip().startswith("#") or comment_section is True:
            continue

        if starting_point_vars:
            if "def" in line and "(" in line:
                ending_point_vars = True

        if starting_point_class and starting_point_vars and not ending_point_vars:
            vars = line.strip().split()
            if len(vars) == 2:
                vars_type.append(vars[0].strip())
                vars_name.append(vars[1].strip())

        if line.startswith("cdef class"):
            starting_point_class = True

        if starting_point_class is True and line.startswith(" ") and "cdef:" in line:
            starting_point_vars = True

    assert len(vars_type) == len(vars_name)

    # for var in zip(vars_type, vars_name):
    #    print(var[0],var[1])

    ###############

    basetyp_sql_bool = "BOOL"
    basetyp_sql_int = "INT"
    basetyp_sql_double = "DOUBLE"
    basetyp_sql_float = "FLOAT"
    basetyp_sql_string = f"VARCHAR({default_strlen})"
    basetyp_sql_enum = "SMALLINT UNSIGNED"

    default_unsigned = False
    default_notnull = False

    for s in [s for s in sql_rules if s.lstrip().startswith("convert")]:
        # print(s)
        slist = s.split(" ")
        if len(slist) != 3:
            continue
        if slist[1] == "bool":
            basetyp_sql_bool = slist[2].strip()
        if slist[1] == "int":
            basetyp_sql_int = slist[2].strip()
        if slist[1] == "double":
            basetyp_sql_double = slist[2].strip()
        if slist[1] == "float":
            basetyp_sql_double = slist[2].strip()
        if slist[1] == "enum":
            basetyp_sql_double = slist[2].strip()
        if slist[1] == "str" or slist[1] == "string":
            basetyp_sql_string = slist[2].strip()

    if "default unsigned" in sql_rules:
        default_unsigned = True
        basetyp_sql_int += " UNSIGNED"
        basetyp_sql_double += " UNSIGNED"
        basetyp_sql_float += " UNSIGNED"

    if "default notnull" in sql_rules:
        default_notnull = True
        basetyp_sql_int += " NOT NULL"
        basetyp_sql_double += " NOT NULL"
        basetyp_sql_float += " NOT NULL"
        basetyp_sql_string += " NOT NULL"
        basetyp_sql_enum += " NOT NULL"

    basetypes = ["bool", "int", "double", "float", "enum", "str", "string"]

    basetypes_sql = [
        basetyp_sql_bool,
        basetyp_sql_int,
        basetyp_sql_double,
        basetyp_sql_float,
        basetyp_sql_enum,
        basetyp_sql_string,
        basetyp_sql_string,
    ]

    assert len(basetypes) == len(basetypes_sql)

    ############

    var_appendings = []

    vars_notnull = []
    vars_unsigned = []

    for rule in sql_rules:
        if rule.startswith("unsigned") and default_unsigned is False:
            if len(vars_unsigned) > 0:
                raise RuntimeError(
                    f'multiple defines of "# sqlcon: unsigned" in {filename_pxd}'
                )

            vlist = rule.split(" ")
            if len(vlist) > 1:
                for v in vlist:
                    if v.strip() != "unsigned":
                        vars_unsigned.append(v.strip())

        if rule.startswith("notnull") and default_notnull is False:
            if len(vars_notnull) > 0:
                raise RuntimeError(
                    f'multiple defines of "# sqlcon: notnull" in {filename_pxd}'
                )

            vlist = rule.split(" ")
            if len(vlist) > 1:
                for v in vlist:
                    if v.strip() != "notnull":
                        vars_notnull.append(v.strip())

    for v in vars_name:
        if v in vars_notnull:
            if v in vars_unsigned:
                var_appendings.append("UNSIGNED NOT NULL")
            else:
                var_appendings.append("NOT NULL")
        elif v in vars_unsigned:
            var_appendings.append("NOT NULL")
        else:
            var_appendings.append("")

    assert len(var_appendings) == len(vars_name)

    vars_sqltype = []
    remove_position = []
    for idx, v in enumerate(vars_type):
        # print(idx)

        if v in enums:
            v = "enum"
        try:
            idx_var = basetypes.index(v)
            vars_sqltype.append(
                (basetypes_sql[idx_var] + " " + var_appendings[idx]).strip()
            )
        except ValueError:
            print(
                f'\tInfo: var type "{v}" not defined in basetypes list.',
                f'\tVar "{vars_name[idx]}" excluded from SQL Statement.',
            )
            remove_position.append(idx)

    for pos in reversed(remove_position):
        del var_appendings[pos]
        del vars_name[pos]
        del vars_type[pos]
    # print(vars_sqltype)

    #########

    # Generate strings to write SQL statements

    # save
    str_save = f"""
    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS {sqltablename}("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "\n"""

    for idx, name in enumerate(vars_name):
        comma = ", "
        if idx + 1 == len(vars_name):
            comma = ")"
        str_save += f'            + "{name} {vars_sqltype[idx]}{comma}"\n'

        if idx + 1 == len(vars_name):
            str_save += "        )\n"

    str_save += "\n        cursor.execute(sqlquery)\n\n"
    str_save += f"""        sqlquery = (
            "INSERT INTO {sqltablename} VALUES ("\n"""
    str_save += '            + str(self.id) + ", "\n'

    for idx, name in enumerate(vars_name):
        comma = ", "
        if idx + 1 == len(vars_name):
            comma = ")"
        str_save += f'            + str(self.{name}) + "{comma}"\n'

        if idx + 1 == len(vars_name):
            str_save += "        )\n"

    str_save += "\n        cursor.execute(sqlquery)\n"
    str_save += "\n        db.commit()\n"

    # load
    str_load = f"""
    @staticmethod
    cdef {classname} load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM {sqltablename}" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        \n"""

    for idx, name in enumerate(vars_name):
        if vars_type[idx] == "str" or vars_type[idx] == "string":
            str_load += f"        cdef str {name} = row[{idx+1}]\n"
        else:
            str_load += f"        cdef {vars_type[idx]} {name} = <{vars_type[idx]}>row[{idx+1}]\n"

    str_load += f"\n        return {classname}(\n"
    str_load += "            id,\n"
    str_load += "            core,\n"

    for idx, name in enumerate(vars_name):
        comma = ", "
        if idx + 1 == len(vars_name):
            comma = ""
        str_load += f"            {name}{comma}\n"

    str_load += "        )"

    # update
    str_update = f"""
    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE {sqltablename} SET "\n"""

    for idx, name in enumerate(vars_name):
        comma = ", "
        if idx + 1 == len(vars_name):
            comma = " "
        str_update += f'            + "{name} = " + str(self.{name}) + "{comma}"\n'
    str_update += '            + "WHERE id = " + str(self.id)\n        )\n\n'
    str_update += "        cursor.execute(sqlquery)\n"
    str_update += "\n        db.commit()\n"

    merged_textblock = (
        "    # last generator run at "
        + datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        + "\n"
        + str_save
        + "\n"
        + str_update
        + "\n"
        + str_load
        + "\n"
    )

    ###################

    # save in files

    startstring = "# autocreated sqlcon function block - do not change"
    endstring = "# autocreated sqlcon function block end - do not change"

    with open(filename_pyx, "r") as input:
        with open(tempfile_pyx, "w") as output:
            lines = input.readlines()
            delete_lines = False
            printed_my_shit = False

            for line in lines:
                if line.lstrip().startswith(endstring):
                    delete_lines = False
                    output.write(merged_textblock)
                    printed_my_shit = True

                if delete_lines is False:
                    output.write(line)

                if line.lstrip().startswith(startstring):
                    delete_lines = True

            if printed_my_shit is False:
                output.write(
                    f"\n    {startstring}\n" + merged_textblock + f"    {endstring}\n"
                )

    os.replace(tempfile_pyx, filename_pyx)


def sqlcon_all_files(basepath, backup_path):
    pxdfiles = glob.glob(basepath + "/*.pxd")
    pyxfiles = glob.glob(basepath + "/*.pyx")

    for file in pxdfiles:
        if file.replace(".pxd", ".pyx") in pyxfiles:
            file = file.replace("\\", "/")
            print("Run sqlcon on pxd/pyx files", file.replace(".pxd", ""))
            basepath, filename = file.rsplit("/", 1)
            sqlcon(basepath + "/", filename.replace(".pxd", ""), backup_path)


if __name__ == "__main__":

    backup_path = "sqlcon_backup"

    if len(sys.argv) > 2:
        # given path and filename
        basepath = sys.argv[1]
        filename = sys.argv[2]
        sqlcon(basepath, filename, backup_path)

    if len(sys.argv) > 1:
        #  only path given, generate all classes
        sqlcon_all_files(sys.argv[1], backup_path)

    # basepath = "../../moneymaker/core/deposit/"
    # filename = "transaction"
