from moneymaker.core.persistance.persist cimport Persist
from moneymaker.core.core cimport Core
cimport lol
from doener import lahmacun


cdef class Example(Persist):

    def __cinit__(
        self,
        int id,
        Core core,
        int from_id,
        int to_id,
        double amount_from,
        double amount_to,
        double amount_from_realized,
        double amount_to_realized,
        int time_send,
        int time_realized,
        int commissions_unit_id,
        double commissions_realized,
        booking_status status,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    ):
        self.id = id
        self.core = core
        self.from_id = from_id
        self.to_id = to_id
        self.amount_from = amount_from
        self.amount_to = amount_to
        self.amount_from_realized = amount_from_realized
        self.amount_to_realized = amount_to_realized
        self.time_send = time_send
        self.time_realized = time_realized
        self.commissions_unit_id = commissions_unit_id
        self.commissions_realized = commissions_realized
        self.status = status
        self.target = target
        self.ordertype = ordertype
        self.order_limit_price = order_limit_price


    # method to create new object
    @staticmethod
    cdef Example make(
        int id,
        Core core,
        int from_id,
        int to_id,
        double amount_from,
        double amount_to,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    ):
        double amount_from_realized = # add default value here
        double amount_to_realized = # add default value here
        int time_send = # add default value here
        int time_realized = # add default value here
        int commissions_unit_id = # add default value here
        double commissions_realized = # add default value here
        booking_status status = # add default value here

        return Example(
            id,
            core,
            from_id,
            to_id,
            amount_from,
            amount_to,
            amount_from_realized,
            amount_to_realized,
            time_send,
            time_realized,
            commissions_unit_id,
            commissions_realized,
            status,
            target,
            ordertype,
            order_limit_price
        )

    # print object information
    cpdef void print_obj(self):
        print("Example, ID: ", str(self.id))


    # method to link data ids with objects
    cdef void link(self):
        # implement linking here
        pass

    # autocreated sqlcon function block - do not change
    # last generator run at 2022-02-01 08:56:25

    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS example_class("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "
            + "from_id INT UNSIGNED NOT NULL, "
            + "to_id INT UNSIGNED NOT NULL, "
            + "amount_from DOUBLE, "
            + "amount_to DOUBLE, "
            + "amount_from_realized DOUBLE, "
            + "amount_to_realized DOUBLE, "
            + "time_send INT NOT NULL, "
            + "time_realized INT, "
            + "commissions_unit_id INT NOT NULL, "
            + "commissions_realized DOUBLE, "
            + "status SMALLINT UNSIGNED, "
            + "target SMALLINT UNSIGNED, "
            + "ordertype SMALLINT UNSIGNED, "
            + "order_limit_price DOUBLE)"
        )

        cursor.execute(sqlquery)

        sqlquery = (
            "INSERT INTO example_class VALUES ("
            + str(self.id) + ", "
            + str(self.from_id) + ", "
            + str(self.to_id) + ", "
            + str(self.amount_from) + ", "
            + str(self.amount_to) + ", "
            + str(self.amount_from_realized) + ", "
            + str(self.amount_to_realized) + ", "
            + str(self.time_send) + ", "
            + str(self.time_realized) + ", "
            + str(self.commissions_unit_id) + ", "
            + str(self.commissions_realized) + ", "
            + str(self.status) + ", "
            + str(self.target) + ", "
            + str(self.ordertype) + ", "
            + str(self.order_limit_price) + ")"
        )

        cursor.execute(sqlquery)

        db.commit()


    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE example_class SET "
            + "from_id = " + str(self.from_id) + ", "
            + "to_id = " + str(self.to_id) + ", "
            + "amount_from = " + str(self.amount_from) + ", "
            + "amount_to = " + str(self.amount_to) + ", "
            + "amount_from_realized = " + str(self.amount_from_realized) + ", "
            + "amount_to_realized = " + str(self.amount_to_realized) + ", "
            + "time_send = " + str(self.time_send) + ", "
            + "time_realized = " + str(self.time_realized) + ", "
            + "commissions_unit_id = " + str(self.commissions_unit_id) + ", "
            + "commissions_realized = " + str(self.commissions_realized) + ", "
            + "status = " + str(self.status) + ", "
            + "target = " + str(self.target) + ", "
            + "ordertype = " + str(self.ordertype) + ", "
            + "order_limit_price = " + str(self.order_limit_price) + " "
            + "WHERE id = " + str(self.id)
        )

        cursor.execute(sqlquery)

        db.commit()


    @staticmethod
    cdef Example load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM example_class" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        
        cdef int from_id = <int>row[1]
        cdef int to_id = <int>row[2]
        cdef double amount_from = <double>row[3]
        cdef double amount_to = <double>row[4]
        cdef double amount_from_realized = <double>row[5]
        cdef double amount_to_realized = <double>row[6]
        cdef int time_send = <int>row[7]
        cdef int time_realized = <int>row[8]
        cdef int commissions_unit_id = <int>row[9]
        cdef double commissions_realized = <double>row[10]
        cdef booking_status status = <booking_status>row[11]
        cdef booking_target target = <booking_target>row[12]
        cdef booking_ordertype ordertype = <booking_ordertype>row[13]
        cdef double order_limit_price = <double>row[14]

        return Example(
            id,
            core,
            from_id, 
            to_id, 
            amount_from, 
            amount_to, 
            amount_from_realized, 
            amount_to_realized, 
            time_send, 
            time_realized, 
            commissions_unit_id, 
            commissions_realized, 
            status, 
            target, 
            ordertype, 
            order_limit_price
        )
    # autocreated sqlcon function block end - do not change
