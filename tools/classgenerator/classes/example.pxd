from moneymaker.core.persistance.persist cimport Persist
from moneymaker.core.core cimport Core
cimport lol
from doener import lahmacun

# build sqlcon
# sqlcon: dbtablename example_class
# sqlcon: convert str VARCHAR(16)
# sqlcon: notnull from_id to_id time_send
# sqlcon: unsigned from_id to_id commissions_unit_id

cpdef enum booking_status:
    # implement enum here
    # undefined = 0 ..

cpdef enum booking_target:
    # implement enum here
    # undefined = 0 ..

cpdef enum booking_ordertype:
    # implement enum here
    # undefined = 0 ..


cdef class Example(Persist):

    cdef:
        int from_id
        int to_id
        double amount_from
        double amount_to
        double amount_from_realized
        double amount_to_realized
        int time_send
        int time_realized
        int commissions_unit_id
        double commissions_realized
        booking_status status
        booking_target target
        booking_ordertype ordertype
        double order_limit_price


    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef Example load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef Example make(
        int id,
        Core core,
        int from_id,
        int to_id,
        double amount_from,
        double amount_to,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    )

    # print object information
    cpdef void print_obj(self)

    # method to link data ids with objects
    cdef void link(self)
