from pathlib import Path


class_filename = "example"
filename_template = f"tools/classgenerator/templates/{class_filename}.template"


file1 = open(filename_template, "r")
lines = file1.readlines()
file1.close()


def get_cat(s: str) -> str:
    if s.lstrip().startswith("#"):
        return s.replace("#", "").strip()
    else:
        return ""


def is_id(s: str):
    splitter = s.split(" ")
    if len(splitter) == 2:
        if splitter[1].strip() == "id":
            return True
    return False


cat = "none"
classname = ""
vars = []
ctor_vars = []
sqlcon_commands = []
imports = []
enums = []

for line in lines:
    if line.strip() == "":
        continue

    cat_var = get_cat(line)
    if cat_var != "":
        cat = cat_var
        continue
    if cat == "classname":
        classname = line.replace(" ", "").strip().title()
        print("classname", classname)
    elif cat == "imports":
        if line.strip().startswith(("from", "import", "cimport")):
            imports.append(line.strip())
    elif cat == "enums":
        enums.append(line.strip())
    elif cat == "variables":
        if is_id(line):
            continue
        vars.append(line.strip())
    elif cat == "ctor variables":
        if is_id(line):
            continue
        ctor_vars.append(line.strip())
    elif cat == "build sqlcon":
        sqlcon_commands.append(line.strip())


# filter and reorder ctor_vars
# only take vars that are in c_tor vars
ctor_vars_filtered = [v for v in vars if v in ctor_vars]


pxdfile = ""

pxdfile += "from moneymaker.core.persistance.persist cimport Persist\n"
pxdfile += "from moneymaker.core.core cimport Core\n"

for ln in imports:
    pxdfile += ln + "\n"

pxdfile += "\n"

if len(sqlcon_commands) > 0:
    pxdfile += "# build sqlcon\n"

    for ln in sqlcon_commands:
        pxdfile += "# " + ln + "\n"

    pxdfile += "\n"

for ln in enums:
    pxdfile += f"cpdef enum {ln}:\n"
    pxdfile += "    # implement enum here\n"
    pxdfile += "    # undefined = 0 ..\n\n"


pxdfile += f"""
cdef class {classname}(Persist):

    cdef:
"""

for ln in vars:
    pxdfile += "        " + ln + "\n"

pxdfile += f"""

    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef {classname} load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef {classname} make(
        int id,
        Core core,\n"""

for idx, ln in enumerate(ctor_vars_filtered):
    if idx + 1 == len(ctor_vars_filtered):
        comma = ""
    else:
        comma = ","

    pxdfile += "        " + ln + comma + "\n"

pxdfile += "    )\n"
pxdfile += """
    # print object information
    cpdef void print(self)

    # method to link data ids with objects
    cdef void link(self)
"""

pyxfile = ""

pyxfile += "from moneymaker.core.persistance.persist cimport Persist\n"
pyxfile += "from moneymaker.core.core cimport Core\n"

for ln in imports:
    pyxfile += ln + "\n"

pyxfile += "\n"
pyxfile += f"""
cdef class {classname}(Persist):

    def __cinit__(
        self,
        int id,
        Core core,\n"""

for idx, ln in enumerate(vars):
    if idx + 1 == len(vars):
        comma = ""
    else:
        comma = ","

    pyxfile += "        " + ln + comma + "\n"

pyxfile += "    ):\n"
pyxfile += "        self.id = id\n"
pyxfile += "        self.core = core\n"

for ln in vars:
    # unit = ln.split(" ")[0]
    var = ln.split(" ")[1]

    pyxfile += f"        self.{var} = {var}\n"


pyxfile += f"""\n
    # method to create new object
    @staticmethod
    cdef {classname} make(
        int id,
        Core core,\n"""

for idx, ln in enumerate(ctor_vars_filtered):
    if idx + 1 == len(ctor_vars_filtered):
        comma = ""
    else:
        comma = ","

    pyxfile += "        " + ln + comma + "\n"

pyxfile += "    ):\n"

for ln in vars:
    if ln in ctor_vars_filtered:
        continue

    pyxfile += f"        {ln} = # add default value here\n"

pyxfile += f"\n        return {classname}(\n            id,\n"
pyxfile += "            core,\n"
for idx, ln in enumerate(vars):
    if idx + 1 == len(vars):
        comma = ""
    else:
        comma = ","

    varname = ln.split(" ")[1]
    pyxfile += f"            {varname}{comma}\n"

pyxfile += "        )"

pyxfile += "\n"

pyxfile += f"""
    # print object information
    cpdef void print(self):
        print("{classname}, ID: ", str(self.id))


    # method to link data ids with objects
    cdef void link(self):
        # implement linking here
        pass
"""
# print(pyxfile)


Path("classes").mkdir(parents=True, exist_ok=True)

with open(f"tools/classgenerator/classes/{class_filename}.pxd", "w") as output:
    output.write(pxdfile)

with open(f"tools/classgenerator/classes/{class_filename}.pyx", "w") as output:
    output.write(pyxfile)
