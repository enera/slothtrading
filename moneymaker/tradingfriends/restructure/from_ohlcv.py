from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
import pandas as pd


class FromOHLCV(BaseEstimator, TransformerMixin):
    """
    Get custome data format from ohlcv
    """

    def __init__(self, structure="c", df=True):
        """Args: structure: Use costume string with symbols o,h,l,c,v for columns
        Example: "cv" returns two columns with close and volume
        """

        self.structure = structure
        self.df = df

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        num_rows, num_cols = X.shape
        assert num_cols >= 5

        if self.df:
            assert isinstance(X, pd.DataFrame)

        if isinstance(X, pd.DataFrame):
            pos_o = np.nonzero(X.columns.str.match("open", case=False))[0][0]
            pos_h = np.nonzero(X.columns.str.match("high", case=False))[0][0]
            pos_l = np.nonzero(X.columns.str.match("low", case=False))[0][0]
            pos_c = np.nonzero(X.columns.str.match("close", case=False))[0][0]
            pos_v = np.nonzero(X.columns.str.match("volume", case=False))[0][0]

            positions = {"o": pos_o, "h": pos_h, "l": pos_l, "c": pos_c, "v": pos_v}
        else:
            positions = {"o": 0, "h": 1, "l": 2, "c": 3, "v": 4}

        cols = []

        for chr in self.structure:
            if isinstance(X, pd.DataFrame):
                if self.df:
                    cols.append(X.iloc[:, positions[chr]])
                else:
                    cols.append(X.values[:, positions[chr]])
            else:
                cols.append(X[:, positions[chr]])

        if self.df:
            return pd.concat(cols, axis=1)

        return np.vstack(cols)
