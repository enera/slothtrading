from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
import pandas as pd


class FromSharadarToOHLCV(BaseEstimator, TransformerMixin):
    """
    Convert and adjust sharadar data to ohlcv data.
    """

    def __init__(self, copy=True):
        self.copy = copy

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        num_rows, num_cols = X.shape
        assert num_cols >= 5
        assert isinstance(X, pd.DataFrame)
        if self.copy:
            df = X.copy()
        else:
            df = X
        df.sort_values("date", inplace=True)
        df.set_index("date", inplace=True)
        df["open"] = df["open"] * df["closeadj"] / df["close"]
        df["high"] = df["high"] * df["closeadj"] / df["close"]
        df["low"] = df["low"] * df["closeadj"] / df["close"]
        df["close"] = df["close"] * df["closeadj"] / df["close"]
        return df.loc[:, ["open", "high", "low", "close", "volume"]]
