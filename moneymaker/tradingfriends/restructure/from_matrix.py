from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class FromMatrix(BaseEstimator, TransformerMixin):
    """
    Get custome cols format dataframe or 2D array, adressable via col names or numbers
    """

    def __init__(self, col_names="", cols=[], df=False):
        """Args: col_names: Name of columns (use only with df=True)
              cols: Indices of wished cols

        Example: "cv" returns two columns with close and volume
        """

        self.col_names = col_names
        self.cols = cols
        self.df = df

        if df is False:
            assert len(cols) > 0
            assert not col_names

        assert not (col_names and len(cols) > 0)

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        num_rows, num_cols = X.shape
        assert num_cols >= 5

        if self.df:
            assert isinstance(X, pd.DataFrame)
            if self.col_names:
                return X.loc[:, self.col_names]
            else:
                return X.iloc[:, self.cols]

        else:
            return X[:, self.cols]
