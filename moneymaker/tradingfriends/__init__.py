"""
Rules for tradingfriends classes:

* Inherit from sklearn.base.BaseEstimator and .TransformerMixin
* If possible, only use sklearn, pandas and numpy
* Implement at least init, fit and transform methods
* If passed object is a df, return a df, else return a 2D array
* Use following subfolders:
    * cleaner: if class deals with data cleaning, filling of missing values etc
    * decomposer: if class decomposes data in different components
    * generator: if new (pseudo)data is generated (to be used as initial step in pipeline)
    * indicator: if data is processed and manipulated
    * resampler: if data is resampled for a different timeframe or non timebased sampling methods
    * restructure: if specific columns are to be selected
    
"""