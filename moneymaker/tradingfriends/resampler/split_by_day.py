from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class SplitByDay(BaseEstimator, TransformerMixin):
    """
    Split data into daily chunks returned as list
    X must be a Dataframe
    Index must be datetime-like
    """

    def __init__(self):
        return

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """
        Split dataframe into daily chunks returned as list
            Index must be datetime-like
        """
        assert isinstance(X, pd.DataFrame) or isinstance(X, pd.Series)
        return [group[1] for group in X.groupby(X.index.date)]
