from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import numpy as np
import datetime


class RandomOHLCV(BaseEstimator, TransformerMixin):
    """
    Calculate volume weighted average price
    """

    def __init__(self, seed=42, start_price=1, freq="1min", remove_weekends=True):
        self.seed = seed
        self.start_price = np.abs(start_price)
        self.freq = freq
        if remove_weekends:
            self.max_days = 5
        else:
            self.max_days = 7
        return

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """X is number of days (int)"""

        days = X
        seconds = days * 24 * 60 * 60

        # create 1 second ticks of returns
        np.random.seed(self.seed)

        df = pd.DataFrame(
            {"price": np.power(np.random.normal(0, 2e-2, size=seconds), 3)},
            index=pd.date_range(
                start="2021-01-01", periods=seconds + 1, freq="1s", name="timeidx"
            )[1:],
        )

        # build candles and add volume
        np.random.seed(self.seed)

        return (
            df.assign(price=df.price.add(1).cumprod())
            .mul(self.start_price)
            .query(
                "(timeidx.dt.time < datetime.time(16,00)) & (timeidx.dt.time > datetime.time(9,30))"
            )
            .query(f"timeidx.dt.weekday < {self.max_days}")
            .resample(self.freq)
            .ohlc()
            .dropna()
            .droplevel(0, axis=1)
            .assign(
                volume=lambda x: (np.random.gamma(2, size=len(x)) * 100).astype(int)
            )
            .assign(
                volume=lambda df: df.volume.where(
                    df.index.time != datetime.time(9, 30), df.volume * 10
                )
            )
            .assign(
                volume=lambda df: df.volume.where(
                    df.index.time != datetime.time(15, 59), df.volume * 5
                )
            )
        )
