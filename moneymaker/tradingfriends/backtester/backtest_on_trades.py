from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import numpy as np


class BacktestOnTrades(BaseEstimator, TransformerMixin):
    """
    Creates trades table from signal time series
    """

    def __init__(self, allow_shorting: bool = True, trading_fee_pct: float = 0.0):
        """
        Params:
        allow_shorting: Whether short positions are allowed
        """
        self.allow_shorting = allow_shorting
        self.trading_fee_pct = trading_fee_pct

    def fit(self, X, y=None):
        return self

    def transform(self, X: pd.DataFrame):

        """
        expected structure of X:
            [
            "start",
            "end",
            "signal",
            "start price",
            "end price",
            "stock change abs",
            "stock change rel",
            ]
        """
        X["trade return"] = X["stock change rel"] * X["signal"] - self.trading_fee_pct

        return X
