from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class BacktestOnSignal(BaseEstimator, TransformerMixin):
    """
    Trades amount of capital as given by X[:,1] (value=1: 100% long)
    Trade if X[:,1] > 0, short if X[:,1] < 0, flat if X[:,1] == 0
    col 0 is price, col 1 is signal
    """

    def __init__(
        self,
        trading_fee_pct: float = 0.0,
        buy_on: str = "same",
        add_buy_and_hold: bool = False,
        allocation_factor: float = 1.0,
    ):
        """
        Params: trading_fee_pct: percentage of fee and spread per trade
                buy_on: "same": same bar as signal, "next": next bar
                add_buy_and_hold: add col with buy and hold performance
                allocation_factor: fraction of capital allocated to strat,
                                   > 1 means leverage

        """
        self.trading_fee_pct = trading_fee_pct

        if buy_on == "same":
            self.shift = 1
        else:  # "next"
            self.shift = 2

        self.add_buy_and_hold = add_buy_and_hold
        self.allocation_factor = allocation_factor

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        df = X

        if not isinstance(X, pd.DataFrame):
            df = pd.DataFrame(X)

        price = df.iloc[:, 0]
        signal = df.iloc[:, 1]

        assert len(X.columns) > 1, "Too few columns"
        assert self.trading_fee_pct >= 0.0, "Trading fee must be non negative"

        return_factor = (
            price.pct_change()
            .fillna(0.0)
            .mul(signal.shift(self.shift).fillna(0.0))
            .add(1)
            .cumprod()
            .mul(self.allocation_factor)
            .add(1 - self.allocation_factor)
        )

        if self.trading_fee_pct > 0.0:
            return_factor = return_factor.add(
                signal.shift(self.shift)
                .diff()
                .abs()
                .fillna(0.0)
                .mul(-return_factor)
                .mul(self.trading_fee_pct / 100)
                .cumsum()
            )

        # check if strategy is 0 somewhere and set to 0 afterwards (margin call lol)
        idx_le0 = return_factor.le(0).idxmax()
        if idx_le0 != return_factor.index[0]:
            return_factor.loc[idx_le0:] = 0.0

        if self.add_buy_and_hold is True:
            return pd.concat(
                [
                    return_factor,
                    price.pct_change().fillna(0.0).add(1).cumprod(),
                ],
                axis=1,
            ).set_axis(["strategy", "buy and hold"], axis=1)
        else:
            return return_factor.to_frame().set_axis(["strategy"], axis=1)
