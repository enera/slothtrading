from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class TradesFromSignal(BaseEstimator, TransformerMixin):
    """
    Creates trades table from signal time series
    """

    def __init__(
        self,
        buy_on: str = "same",
    ):
        """
        Params: buy_on: "same": same bar as signal, "next": next bar
        """
        if buy_on == "same":
            self.shift = 0
        else:  # "next"
            self.shift = 1

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        # calc trades
        # algorithm adds a col with rownum (idx_pos)
        # filters for rows with trades
        # and finally calcs trade durations
        # by filtering for trade series with idx_pos diffs > 1

        assert is_pandas(X)
        print(X)
        assert len(X.columns) == 2

        price = X.iloc[:, 0]
        signal = X.iloc[:, 1]

        signal_with_idxpos = (
            signal.fillna(0)
            .shift(self.shift)
            .reset_index()
            .reset_index()
            .set_index("Date")
            .set_axis(["idx_pos", "signal"], axis=1)
        )

        signal_vals = signal.drop_duplicates().values
        print(signal_vals)
        trades_per_group = []

        for group_val in signal_vals:
            if group_val == 0:
                continue

            start = (
                signal_with_idxpos.query(f"signal=={group_val}")
                .diff()
                .fillna(2)
                .query("idx_pos>1")
                .index
            )
            end = (
                signal_with_idxpos.query(f"signal=={group_val}")
                .diff()
                .shift(-1)
                .fillna(2)
                .query("idx_pos>1")
                .index
            )

            # close last trade
            if len(start) > len(end):
                print("appending end")
                end = end.append(signal.iloc[-2:-1].index)

            # if end is out of bounds, reset end to last possible date/time
            # at this point, this is -2, because end is getting idx+1 later
            end_temp = end.to_frame()
            end_temp[end_temp > signal.index[-2]] = signal.index[-2]
            end = end_temp.set_index("Date").index

            # move end stamp one timestamp forward (idx+1)
            end = signal.iloc[(signal_with_idxpos.loc[end].idx_pos + 1).values].index

            df = pd.DataFrame({"start": start, "end": end, "signal": group_val})

            # remove trades with same start and end time (may occur for last trades)
            df = df.loc[~(df.start == df.end)]

            # assert that all trades are correct orientated
            assert (
                df.end > df.start
            ).all(), "End time before start time for at least one trade"

            trades_per_group.append(df)

        final_cols = [
            "start",
            "end",
            "signal",
            "start price",
            "end price",
            "stock change abs",
            "stock change rel",
        ]

        trades = pd.concat(trades_per_group)

        if len(trades) == 0:
            return pd.DataFrame(columns=final_cols)

        price_start = price.loc[trades.iloc[:, 0]].reset_index(drop=True)
        price_end = price.loc[trades.iloc[:, 1]].reset_index(drop=True)

        absolute_change = price_end - price_start
        relative_change = absolute_change / price_start

        return pd.concat(
            [trades, price_start, price_end, absolute_change, relative_change],
            axis=1,
            ignore_index=True,
        ).set_axis(final_cols, axis=1)
