import pandas as pd
import numpy as np


class BacktestMetrics:
    def __init__(
        self,
        signal: pd.DataFrame,
        strategy: pd.DataFrame,
        price: pd.DataFrame,
        shift: int = -1,
    ):

        assert len(signal.columns) == 1
        assert len(strategy.columns) == 1
        assert len(price.columns) == 1
        assert len(signal) == len(strategy)
        assert len(price) == len(strategy)
        assert len(signal) > 0

        self.signal = signal.set_axis(["signal"], axis=1)
        self.strategy = strategy.set_axis(["strategy"], axis=1)
        self.price = price.set_axis(["price"], axis=1)
        self.shift = shift

    #  A -- Full time series based metrics ---

    def sharpe_ratio(self, period=252):
        pct_ret = self.strategy.pct_change().dropna()
        return np.sqrt(period) * pct_ret.mean() / pct_ret.std()

    def sortino_ratio(self, period=252):
        pct_ret = self.strategy.pct_change().dropna()
        pct_ret_neg = pct_ret.copy()
        pct_ret_neg[pct_ret_neg.strategy > 0] = 0
        return np.sqrt(period) * pct_ret.mean() / pct_ret_neg.std()

    def underwater_plot(self):
        return self.strategy - self.strategy.cummax()

    def max_drawdown(self):
        return (self.underwater_plot()).min().abs()[0]

    def duration_max_drawdown(self):
        return (
            self.underwater_plot()
            .set_axis(["dd"], axis=1)
            .query("dd==0")
            .reset_index()
            .iloc[:, 0]
            .diff()
            .max()
        )

    def time_invested(self):
        return (
            self.signal.set_axis(["signal"], axis=1).query("signal!=0").count()
            / len(self.signal)
        )[0]

    #  B -- Trade based metrics ---

    def calc_trades(self):

        # calc trades
        # algorithm adds a col with rownum (idx_pos)
        # filters for rows with trades
        # and finally calcs trade durations
        # by filtering for trade series with idx_pos diffs > 1

        signal_with_idxpos = (
            self.signal.shift(-self.shift)
            .fillna(0)
            .reset_index()
            .reset_index()
            .set_index("Date")
            .set_axis(["idx_pos", "signal"], axis=1)
        )

        signal_vals = self.signal.iloc[:, 0].drop_duplicates().values
        trades_per_group = []

        for group_val in signal_vals:
            if group_val == 0:
                continue

            start = (
                signal_with_idxpos.query(f"signal=={group_val}")
                .diff()
                .fillna(2)
                .query("idx_pos>1")
                .index
            )
            end = (
                signal_with_idxpos.query(f"signal=={group_val}")
                .diff()
                .fillna(2)
                .shift(-1)
                .query("idx_pos>1")
                .index
            )

            # close last trade
            if len(start) > len(end):
                end = end.append(self.signal.iloc[-1 + self.shift : self.shift].index)

            # move end stamp one timestamp forward
            end = self.signal.iloc[
                (signal_with_idxpos.loc[end].idx_pos + 1).values
            ].index

            trades_per_group.append(
                pd.DataFrame({"start": start, "end": end, "signal": group_val})
            )

        return pd.concat(trades_per_group)

    def _is_trades_passed(self, trades):
        if trades is None:
            # if trades is not passed to function, calc
            trades = self.calc_trades()
        assert len(trades.columns) == 3
        return trades

    def calc_trades_mean_length(self, trades: pd.DataFrame = None):
        trades = self._is_trades_passed(trades)
        return (trades.end - trades.start).mean()

    def calc_num_trades(self, trades: pd.DataFrame = None):
        trades = self._is_trades_passed(trades)
        return len(trades)

    def calc_num_trades_per_year(self, trades: pd.DataFrame = None):
        trades = self._is_trades_passed(trades)
        total_trades = len(trades)
        total_days = (trades.iloc[-1, 1] - trades.iloc[0, 0]).days
        if total_days == 0:
            total_days = 1
        return total_trades / total_days * 365

    #  C -- Trade return based metrics ---

    def calc_trades_profit(self, trades: pd.DataFrame = None):
        trades = self._is_trades_passed(trades)

        final_cols = [
            "start",
            "end",
            "signal",
            "stock change abs",
            "stock change rel",
            "return per trade",
        ]

        if len(trades) == 0:
            return pd.DataFrame(columns=final_cols)

        price_end = self.price.loc[trades.iloc[:, 1]].reset_index(drop=True)
        price_start = self.price.loc[trades.iloc[:, 0]].reset_index(drop=True)

        absolute_change = price_end - price_start
        relative_change = absolute_change / price_start
        return_per_trade = relative_change.iloc[:, 0] * (
            trades.iloc[:, 2].reset_index(drop=True)
        )

        return pd.concat(
            [trades, absolute_change, relative_change, return_per_trade],
            axis=1,
            ignore_index=True,
        ).set_axis(final_cols, axis=1)

    def _is_trades_passed_with_profit_cols(self, trades_profit):
        if trades_profit is None:
            # if trades is not passed to function, calc
            trades_profit = self.calc_trades_profit()
        assert len(trades_profit.columns) == 6
        return trades_profit

    def calc_trades_profit_metrics(self, trades_profit: pd.DataFrame = None):
        trades_profit = self._is_trades_passed_with_profit_cols(trades_profit)
        return_per_trade = trades_profit.loc[:, "return per trade"]
        return return_per_trade.describe()
