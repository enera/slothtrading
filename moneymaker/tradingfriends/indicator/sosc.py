from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class SOSC(BaseEstimator, TransformerMixin):
    """
    Calculate stochastic oscillator
    """

    def __init__(self, minmaxwindow=14, signalwindow=3):
        self.minmaxwindow = minmaxwindow
        self.signalwindow = signalwindow

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X)

        high = temp.rolling(self.minmaxwindow).max()
        low = temp.rolling(self.minmaxwindow).min()
        k = (temp - low) / (high - low)
        d = k.rolling(window=self.signalwindow).mean()

        if is_pandas(X):
            return (k - d).set_axis(["stoch. osc"], axis=1, inplace=False)
        else:
            return (k - d).values
