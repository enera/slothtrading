from sklearn.base import BaseEstimator, TransformerMixin


class Wrapper(BaseEstimator, TransformerMixin):
    """
    Wrapper class for a fast generation custom pipeline functions
    """

    def __init__(self, func, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.func = func

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return self.func(X,*self.args, **self.kwargs)
