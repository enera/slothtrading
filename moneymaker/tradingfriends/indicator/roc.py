from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class RateOfChange(BaseEstimator, TransformerMixin):
    """
    Calculate the the rate of change
    """

    def __init__(self, window, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X).pct_change(self.window, *self.args, **self.kwargs)

        return temp.set_axis(["roc"], axis=1, inplace=False)
