from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class IsLargestCol(BaseEstimator, TransformerMixin):
    """
    Returns 1 if given col is largest, 0 else
    """

    def __init__(self, colidx=-1, colname="", max=1, min=0):
        """
        Args:
            colidx (int) : index of col to check for largest val
            colname (str): name of col to check for largest val
            max (float): value if condition is true (default 1)
            min (float): value if condition is false (default 0)
        """
        self.colname = colname
        self.colidx = colidx
        self.max = max
        self.min = min
        assert not (colname and colidx >= 0), "colname xor colidx needed, both given"

    def fit(self, X, y=None):
        return self

    def transform(self, X: pd.DataFrame):

        assert isinstance(X, pd.DataFrame)
        assert len(X.columns) > 1, "Too few columns"

        colidx = self.colidx

        if self.colname:
            assert self.colname in X.columns, "colname not in columns of df"
            colidx = X.columns.get_loc(self.colname)

        assert len(X.columns) > colidx

        # assert unique col names
        X.columns = range(len(X.columns))

        return (
            (X.idxmax(axis=1) == X.columns[colidx])
            .astype(int)
            .to_frame()
            .set_axis(["largestcol"], axis=1)
            .mul(self.max - self.min)
            .add(self.min)
        )
