from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class Logic(BaseEstimator, TransformerMixin):
    """
    Calculate rolling low
    """

    def __init__(self, key="AND"):
        """
        possible keys:
        AND : returns 1 if all cols are true, 0 else
        OR: returns 1 if one col is true, 0 else
        XOR: returns 1 if exact one col is true, 0 else
        SUM: returns number of true cols
        """
        self.key = key
        assert key in ["AND", "OR", "XOR", "SUM"], "no valid key"

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        temp = pd.DataFrame(X)

        if self.key == "AND":
            temp = temp.astype(bool).sum(axis=1) == len(temp.columns)
        elif self.key == "OR":
            temp = temp.astype(bool).sum(axis=1).astype(bool)
        elif self.key == "XOR":
            temp = temp.astype(bool).sum(axis=1) == 1
        elif self.key == "SUM":
            temp = temp.astype(bool).sum(axis=1)

        if is_pandas(X):
            return (
                temp.to_frame().astype(int).set_axis([self.key], axis=1, inplace=False)
            )
        else:
            return temp.values
