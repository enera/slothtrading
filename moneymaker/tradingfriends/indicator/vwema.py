from sklearn.base import BaseEstimator, TransformerMixin
from moneymaker.tradingfriends.helper import is_pandas
import pandas as pd


class VWEMA(BaseEstimator, TransformerMixin):
    """
    Calculate simple moving average
    """

    def __init__(self, window, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """Structure: [pv]"""

        num_rows, num_cols = X.shape
        assert num_cols >= 2
        df = pd.DataFrame(X)
        temp = (df.iloc[:, 0] * df.iloc[:, 1]).ewm(
            span=self.window, *self.args, **self.kwargs
        ).mean() / df.iloc[:, 1].ewm(span=self.window, *self.args, **self.kwargs).mean()

        if is_pandas(X):
            return temp.to_frame().set_axis(["vwema"], axis=1, inplace=False)
        else:
            return temp.values
