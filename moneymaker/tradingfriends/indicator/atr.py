from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class ATR(BaseEstimator, TransformerMixin):
    """
    Average true range

    Ensure following format / column order: open, high, low, close
    """

    def __init__(self, window=14, matype="sma"):
        """
        Params: window: moving average window steps
                matype: type of moving average (options: "sma", "ema")
        """
        self.window = window
        self.matype = matype

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X)
        assert len(temp.columns) >= 4
        # open = temp.iloc[:, 0]

        high = temp.iloc[:, 1]
        low = temp.iloc[:, 2]
        close = temp.iloc[:, 3]

        # some data checks
        assert (high >= low).sum() == len(temp)
        assert (high >= close).sum() == len(temp)
        assert (low <= close).sum() == len(temp)

        highlow = high - low
        highclose = high - close.shift()
        lowclose = low - close.shift()

        truerange = (
            pd.concat([highlow, highclose, lowclose], axis=1).max(axis=1).to_frame()
        )

        if self.matype == "sma":
            ret = truerange.rolling(window=self.window).mean()
        else:
            ret = truerange.ewm(span=self.window).mean()

        if is_pandas(X):
            return ret.set_axis(["atr"], axis=1, inplace=False)
        else:
            return ret.values
