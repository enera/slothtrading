from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class MACD(BaseEstimator, TransformerMixin):
    """
    Calculate MACD
    """

    def __init__(self, fastwindow=12, slowwindow=26, signalwindow=9):
        self.fastwindow = fastwindow
        self.slowwindow = slowwindow
        self.signalwindow = signalwindow

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X)
        ewmfast = temp.ewm(span=self.fastwindow, adjust=False).mean()
        ewmslow = temp.ewm(span=self.slowwindow, adjust=False).mean()
        macd = ewmfast - ewmslow
        signal = macd.ewm(span=9, adjust=False).mean()

        if is_pandas(X):
            return (macd - signal).set_axis(["macd"], axis=1, inplace=False)
        else:
            return (macd - signal).values
