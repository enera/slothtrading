from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class FractionOfHigh(BaseEstimator, TransformerMixin):
    """
    Calculate the fraction of the last high
    """

    def __init__(self, window, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = (
            pd.DataFrame(X)
            / pd.DataFrame(X).rolling(self.window, *self.args, **self.kwargs).max()
        )

        return temp.set_axis(["foh"], axis=1, inplace=False)
