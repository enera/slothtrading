from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class EMA(BaseEstimator, TransformerMixin):
    """
    Calculate exponential weighted moving average based on given time span
    """

    def __init__(self, horizon, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.horizon = horizon

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X).ewm(span=self.horizon, *self.args, **self.kwargs).mean()

        if is_pandas(X):
            return temp.set_axis(["ema"], axis=1, inplace=False)
        else:
            return temp.values
