from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import numpy as np
from moneymaker.tradingfriends.helper import is_pandas


class BollingerBands(BaseEstimator, TransformerMixin):
    """
    Calculate stochastic oscillator
    """

    def __init__(self, window=20, stddevs=2, bands="upper"):
        """
        Params:
        window: MA horizon
        stddevs: std deviations of bb
        bands: select which bands get returned in cols (upper, lower, both)
        """
        self.window = window
        self.stddevs = stddevs
        self.bands = bands

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        assert np.shape(X)[1] == 1, "Data has more than one column"

        temp = pd.DataFrame(X)
        roll = temp.rolling(window=self.window)
        s = roll.std()
        m = roll.mean()

        if self.bands == "upper":
            ret = (m + self.stddevs * s).set_axis(["upper bb"], axis=1, inplace=False)
        elif self.bands == "lower":
            ret = (m - self.stddevs * s).set_axis(["lower bb"], axis=1, inplace=False)
        else:
            ret = pd.concat(
                [(m + self.stddevs * s), (m - self.stddevs * s)], axis=1
            ).set_axis(["upper band", "lower band"], axis=1, inplace=False)

        if is_pandas(X):
            return ret
        else:
            return ret.values
