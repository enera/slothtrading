from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.resampler.split_by_day import SplitByDay


class VWAP(BaseEstimator, TransformerMixin):
    """
    Calculate volume weighted average price
    """

    def __init__(self):
        return

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """Structure: [pv], Datatype: dataframe"""

        assert isinstance(X, pd.DataFrame)
        num_rows, num_cols = X.shape
        assert num_cols >= 2

        d = SplitByDay()
        daily_dfs = d.transform(X)
        return (
            pd.concat(self._vwap_on_range(dd) for dd in daily_dfs)
            .to_frame()
            .set_axis(["vwap"], axis=1, inplace=False)
        )

    def _vwap_on_range(self, X):
        return (X.iloc[:, 0] * X.iloc[:, 1]).cumsum() / X.iloc[:, 1].cumsum()
