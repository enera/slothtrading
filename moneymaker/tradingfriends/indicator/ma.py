from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class MA(BaseEstimator, TransformerMixin):
    """
    Calculate simple moving average
    """

    def __init__(self, window, nafill="none", *args, **kwargs):
        self.fill = nafill
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        num_rows, num_cols = X.shape
        assert num_cols == 1, "Only single columns are supported"

        def _ma_single_window(window):
            temp = pd.DataFrame(X).rolling(window, *self.args, **self.kwargs).mean()

            if self.fill == "bfill":
                temp = temp.bfill()
            elif self.fill == "ewm":
                temp.iloc[: window - 1] = (
                    temp.iloc[: window - 1].ewm(span=window).mean()
                )
            return temp

        # create multi cols in case of multi windows given
        if isinstance(self.window, list):
            temp = pd.concat([_ma_single_window(w) for w in self.window], axis=1)
        else:
            temp = _ma_single_window(self.window)

        if is_pandas(X):
            if len(temp.columns) == 1:
                return temp.set_axis(["ma"], axis=1, inplace=False)
            else:
                return temp.set_axis(
                    [f"ma{w}" for w in self.window], axis=1, inplace=False
                )
        else:
            return temp.values
