from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class VWMA(BaseEstimator, TransformerMixin):
    """
    Calculate simple moving average
    """

    def __init__(self, window, nafill="none", *args, **kwargs):
        self.fill = nafill
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        """Structure: [pv]"""

        num_rows, num_cols = X.shape
        assert num_cols >= 2

        temp = (
            pd.DataFrame(X[:, 0] * X[:, 1])
            .rolling(self.window, *self.args, **self.kwargs)
            .mean()
            / pd.DataFrame(X[:, 1])
            .rolling(self.window, *self.args, **self.kwargs)
            .mean()
        )

        if self.fill == "bfill":
            temp = temp.bfill()
        elif self.fill == "ewm":
            temp.iloc[: self.window - 1] = (
                temp.iloc[: self.window - 1].ewm(span=self.window).mean()
            )

        if is_pandas(X):
            return temp.set_axis(["VWMA"], axis=1, inplace=False)
        else:
            return temp.values
