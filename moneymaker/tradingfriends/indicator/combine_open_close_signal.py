from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class CombineOpenCloseSignal(BaseEstimator, TransformerMixin):
    """
    This class provides functionality to combine an open signal and a close
    signal to a single combined state.

    The `open_signal` is either
        -1 recommending to open a short position,
         0 for no recommendation or
         1 recommending to open a long position of the asset.

    The `close_signal` is either
        -1 recommending to close a short position,
         0 for no recommendation or
         1 recommending to close a long position of the asset.

    The `conbined_state` is either
        -1 recommending to hold a short position,
         0 recommending no position at all or
         1 recommending to hold a long position.

    The combined state is basically a representation of the position over time
    using the given `open_signal` and `close_signal`.
    """

    def __init__(
        self,
        open_signal_idx=0,
        close_signal_idx=1,
        open_signal_col="",
        close_signal_col="",
    ):
        """Initialize object to combine an open signal and a close signal to a
        state representation.

        Args:
            open_signal_idx (int, optional): Index of the open signal column.
                If `open_signal_col` and `close_signal_col` are specified, they
                have precedence. Defaults to 0.
            close_signal_idx (int, optional): Index of the close signal column.
                If `open_signal_col` and `close_signal_col` are specified, they
                have precedence. Defaults to 1.
            open_signal_col (str, optional): Name of the open signal column. If
            specified `close_signal_col` has to be specified as well. Defaults
            to "".
            close_signal_col (str, optional): Name of the close signal column.
            If specified `open_signal_col` has to be specified as well.
            Defaults to "".
        """
        self.open_signal_idx = open_signal_idx
        self.close_signal_idx = close_signal_idx
        self.open_signal_col = open_signal_col
        self.close_signal_col = close_signal_col

    def fit(self, X, y=None):
        return self

    def transform(self, X: pd.DataFrame):
        assert isinstance(X, pd.DataFrame)
        assert len(X.columns) >= 2, "Too few columns"

        if self.open_signal_col and self.close_signal_col:
            assert (
                self.open_signal_col in X.columns
            ), "open_signal_col not in columns of df"
            assert (
                self.close_signal_col in X.columns
            ), "close_signal_col not in columns of df"

            self.open_signal_idx = X.columns.get_loc(self.open_signal_col)
            self.close_signal_idx = X.columns.get_loc(self.close_signal_col)
        else:
            self.open_signal_col = X.columns[self.open_signal_idx]
            self.close_signal_col = X.columns[self.close_signal_idx]

        combined_state = []
        current_position = 0
        for _, row in X.iterrows():
            if current_position == 1 and row[self.close_signal_col] == 1:
                current_position = 0
            elif current_position == -1 and row[self.close_signal_col] == -1:
                current_position = 0
            elif (
                current_position == 0
                and row[self.open_signal_col] != 0
                and row[self.open_signal_col] != row[self.close_signal_col]
            ):
                current_position = row[self.open_signal_col]
            combined_state.append(current_position)

        return pd.DataFrame({"combined_state": combined_state}, index=X.index)
