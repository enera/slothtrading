from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class Low(BaseEstimator, TransformerMixin):
    """
    Calculate rolling low
    """

    def __init__(self, window, nafill="none", *args, **kwargs):
        self.fill = nafill
        self.args = args
        self.kwargs = kwargs
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X).rolling(self.window, *self.args, **self.kwargs).min()

        if self.fill == "bfill":
            temp = temp.bfill()

        if is_pandas(X):
            return temp.set_axis(["low"], axis=1, inplace=False)
        else:
            return temp.values
