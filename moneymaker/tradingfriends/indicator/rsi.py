from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from moneymaker.tradingfriends.helper import is_pandas


class RSI(BaseEstimator, TransformerMixin):
    """
    Calculate relative strength index
    """

    def __init__(self, window=14):
        self.window = window

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        temp = pd.DataFrame(X)

        delta = temp.diff()
        up = delta.clip(lower=0)
        down = -1 * delta.clip(upper=0)
        ema_up = up.ewm(span=self.window, adjust=False).mean()
        ema_down = down.ewm(span=self.window, adjust=False).mean()

        if is_pandas(X):
            return (ema_up / ema_down).set_axis(["rsi"], axis=1, inplace=False)
        else:
            return (ema_up / ema_down).values
