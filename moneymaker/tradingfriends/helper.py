import pandas as pd


def is_pandas(object):
    return isinstance(object, pd.DataFrame) or isinstance(object, pd.Series)
