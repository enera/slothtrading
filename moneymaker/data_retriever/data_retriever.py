from moneymaker.data_retriever import credentials
import datetime
import urllib
from functools import partial
import os
import pandas as pd
from storefact import get_store_from_url
from kartothek.api.dataset import read_table
from typing import Union


def _construct_azure_url(account: str, container: str, key: str) -> str:
    key_formatted = urllib.parse.quote_plus(key)
    url = f"hazure://{account}:{key_formatted}@{container}"
    return url


def _get_store():
    url = _construct_azure_url(
        credentials.AZURE_ACCOUNT, credentials.AZURE_CONTAINER, credentials.AZURE_KEY
    )
    return partial(get_store_from_url, url)


def get_daily_bars(
    ticker: str,
    start: Union[datetime.date, None] = None,
    end: Union[datetime.date, None] = None,
    security_type: str = "stock",
    folder_path: str = "",
    store_data: bool = False,
) -> pd.DataFrame:
    """
    Retieve daily bars of US securities. Currently only stocks and bonds are supported.

    Args:
        ticker (str): Ticker of the security.
        start (Union[datetime.date, None], optional): Start date to limit the number of
        rows returned (including). If None all data rows are returned without a lower
        limit. Defaults to None.
        end (Union[datetime.date, None], optional): End date to limit the number of
        rows returned (including). If None all data rows are returned without a upper
        limit. Defaults to None.
        security_type (str, optional): Type of the security. Currently only "stock" and
        "bond" are supported. Defaults to "stock".
        folder_path (str, optional): Path to folder to retrieve and store the data to
        limit traffic. If empty no data is retrieved or stored. Defaults to "".
        store_data (bool, optional): If True the data is stored in the specified folder.
        Defaults to False.

    Raises:
        ValueError: Unsupported security type.

    Returns:
        pd.DataFrame: Available daily bars for the specified date range.
    """

    file = os.path.join(folder_path, ticker + ".pkl")
    if folder_path and os.path.isfile(file):
        data = pd.read_pickle(file)
    else:
        if security_type == "stock":
            data = read_table(
                dataset_uuid="sep",
                store=_get_store(),
                predicates=[[("ticker", "==", ticker)]],
            )
        elif security_type == "bond":
            data = read_table(
                dataset_uuid="sfp",
                store=_get_store(),
                predicates=[[("ticker", "==", ticker)]],
            )
        else:
            raise ValueError(f"{security_type} is currently not supported.")

    os.makedirs(folder_path, exist_ok=True)
    if store_data and folder_path:
        data.to_pickle(file)

    if start and end:
        data = data.loc[(data["date"] >= start) & (data["date"] <= end), :]
    elif start:
        data = data.loc[(data["date"] >= start), :]
    elif end:
        data = data.loc[(data["date"] <= end), :]

    return data.copy()


def get_sp500():
    return read_table(dataset_uuid="sp500", store=_get_store())
