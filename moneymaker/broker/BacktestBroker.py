from datetime import datetime
from typing import List, Dict, Union, Tuple
import pandas as pd


class OpenPosition:
    def __init__(
        self,
        ticker: str,
        size: float,
        purchase_price: float,
        purchase_datetime: datetime,
    ) -> None:
        self.purchase_price: float = purchase_price
        self.purchase_datetime: datetime = purchase_datetime
        self.size: float = size
        self.ticker: str = ticker


class ClosedPosition:
    def __init__(
        self,
        ticker: str,
        size: float,
        purchase_price: float,
        purchase_datetime: datetime,
        selling_price: float,
        selling_datetime: datetime,
    ) -> None:
        self.ticker: str = ticker
        self.size: float = size
        self.purchase_price: float = purchase_price
        self.purchase_datetime: datetime = purchase_datetime
        self.selling_price: float = selling_price
        self.selling_datetime: datetime = selling_datetime

    def __str__(self):
        return f"""
            ----------
            ticker {self.ticker}
            size {self.size}
            purchase_price {self.purchase_price}
            purchase_datetime {self.purchase_datetime}
            selling_price {self.selling_price}
            selling_datetime {self.selling_datetime}
            ----------
        """


class BacktestBroker:
    def __init__(
        self,
        start_capital: float,
        fees: float,
        allow_margin: bool,
        benchmark: Union[str, None] = None,
    ):
        self.cash = start_capital
        self.start_capital = start_capital
        self.fees = fees
        self.allow_margin = allow_margin
        self.open_positions: Dict[str, OpenPosition] = dict()
        self.closed_positions: List[ClosedPosition] = []
        self.buy_next_bar: List[Tuple[str, float]] = []
        self.sell_next_bar: List[Tuple[str, Union[float, None]]] = []
        self.benchmark = benchmark
        self.benchmark_size = 0.0
        self.history: Dict[str, List[float]] = {
            "date": [],
            "cash": [],
            "position_value": [],
            "total_value": [],
            "positions": [],
        }
        if benchmark is not None:
            self.history["benchmark"] = []

    def update_history(
        self, date: pd.Timestamp, next_bar: pd.DataFrame, previous_bar: pd.DataFrame
    ):
        position_value = self.get_value(next_bar, previous_bar, False)
        self.history["date"].append(date)
        self.history["cash"].append(self.cash)
        self.history["position_value"].append(position_value)
        self.history["total_value"].append(position_value + self.cash)
        self.history["positions"].append(len(self.open_positions))
        if self.benchmark is not None:
            if len(self.history["benchmark"]) == 0:
                self.benchmark_size = (
                    self.start_capital
                    / next_bar.loc[self.benchmark, "close"]
                    * (1.0 + self.fees)
                )
            self.history["benchmark"].append(
                next_bar.loc[self.benchmark, "close"]
                * self.benchmark_size
                * (1.0 - self.fees)
            )

    def _sell_position(
        self,
        ticker: str,
        size: Union[float, None],
        selling_price: float,
        selling_datetime: datetime,
    ):
        if size is None or size >= self.open_positions[ticker].size:
            self.closed_positions.append(
                ClosedPosition(
                    self.open_positions[ticker].ticker,
                    self.open_positions[ticker].size,
                    self.open_positions[ticker].purchase_price,
                    self.open_positions[ticker].purchase_datetime,
                    selling_price,
                    selling_datetime,
                ),
            )
            self.cash += (
                selling_price * self.open_positions[ticker].size * (1.0 - self.fees)
            )
            self.open_positions.pop(ticker)
        else:
            self.closed_positions.append(
                ClosedPosition(
                    self.open_positions[ticker].ticker,
                    size,
                    self.open_positions[ticker].purchase_price,
                    self.open_positions[ticker].purchase_datetime,
                    selling_price,
                    selling_datetime,
                ),
            )
            self.cash += selling_price * size * (1.0 - self.fees)
            self.open_positions[ticker].size = self.open_positions[ticker].size - size

    def _buy_position(
        self,
        ticker: str,
        size: float,
        purchase_price: float,
        purchase_datetime: datetime,
    ):
        if self.allow_margin or self.cash >= size * purchase_price * (1 + self.fees):
            self.cash -= size * purchase_price * (1 + self.fees)
            if ticker not in self.open_positions.keys():
                self.open_positions[ticker] = OpenPosition(
                    ticker, size, purchase_price, purchase_datetime
                )
            else:
                new_purchase_price = (
                    self.open_positions[ticker].size
                    * self.open_positions[ticker].purchase_price
                    + size * purchase_price
                ) / (self.open_positions[ticker].size + size)
                new_size = self.open_positions[ticker].size + size
                self.open_positions[ticker].purchase_price = new_purchase_price
                self.open_positions[ticker].size = new_size

    def update(
        self,
        next_bar: pd.DataFrame,
        next_date: pd.Timestamp,
        previous_bar: Union[pd.DataFrame, None],
        previous_date: Union[pd.Timestamp, None],
    ):
        ticker_out_of_universe = set()
        if previous_bar is not None:
            ticker_out_of_universe = set(self.open_positions.keys()) - set(
                next_bar.index
            )
            for ticker in ticker_out_of_universe:
                self._sell_position(
                    ticker, None, previous_bar.loc[ticker, "close"], previous_date
                )

        # sell assets
        for ticker, size in self.sell_next_bar:
            if ticker not in self.open_positions.keys():
                # ticker can go out of universe and is already sold
                if ticker not in ticker_out_of_universe:
                    print(f"{next_date}: {ticker} not in possession, nice try!")
                continue
            self._sell_position(ticker, size, next_bar.loc[ticker, "open"], next_date)
        self.sell_next_bar = []

        # buy assets
        ticker_not_available = set(
            [ticker_size[0] for ticker_size in self.buy_next_bar]
        ) - set(next_bar.index)
        for ticker, size in self.buy_next_bar:
            if ticker in ticker_not_available:
                print(f"{ticker} could not be bought on {next_date}.")
                continue
            self._buy_position(ticker, size, next_bar.loc[ticker, "open"], next_date)
        self.buy_next_bar = []

        # update stats
        self.update_history(next_date, next_bar, previous_bar)

    def liquidate_positions(self, bar: pd.DataFrame, date: pd.Timestamp):
        for ticker in [ticker for ticker in self.open_positions.keys()]:
            self._sell_position(ticker, None, bar.loc[ticker, "close"], date)

    def get_value(
        self, bar: pd.DataFrame, previous_bar: pd.DataFrame, with_cash: bool = True
    ) -> float:
        value = self.cash if with_cash else 0.0
        for ticker in self.open_positions.keys():
            price = (
                bar.loc[ticker, "close"]
                if ticker in bar.index
                else previous_bar.loc[ticker, "close"]
            )
            value += price * self.open_positions[ticker].size * (1 - self.fees)
        return value

    def get_positions(self):
        positions = []
        for ticker in self.open_positions.keys():
            positions.append((ticker, self.open_positions[ticker].size))
        return positions
