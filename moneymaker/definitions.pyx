import os

cdef str ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

cdef str get_root_dir():
    return ROOT_DIR