from moneymaker.universe_creators.universe_creator import universe_creator
from moneymaker.data_retriever.data_retriever import get_sp500
import pandas as pd

from typing import List, Dict, Tuple
import datetime


class sp_500_creator(universe_creator):
    def __init__(self, start: datetime.date, end: datetime.date):
        self.start = start
        self.end = end

    def _get_sp_500_date_tickers(self) -> Dict[str, list]:
        """get all ticker needed for the universe S&P 500"""
        sp_500_df = get_sp500()
        sp_500_date_tickers = dict()
        mask = (
            sp_500_df["action"].isin(["historical", "current"])
            & (sp_500_df["date"] >= self.start)
            & (sp_500_df["date"] <= self.end)
        )

        for date, group in sp_500_df.loc[mask, ["date", "ticker"]].groupby("date"):
            sp_500_date_tickers[date.strftime("%Y-%m-%d")] = (
                group["ticker"].values.tolist().copy()
            )

        return sp_500_date_tickers

    def get_history(self) -> Dict[str, List[Tuple[datetime.date, datetime.date]]]:
        """
        Get the complete history of the universe.

        Returns:
            Dict[str, List[Tuple[datetime.date, datetime.date]]]: The keys of the dict
            represent the tickers of the universe. Each ticker has a list tuples which
            represent date ranges in which the ticker was part of the universe.
        """

        sp_500_date_tickers = self._get_sp_500_date_tickers()
        previous_sp500 = []
        sp_500_ticker_periods = dict()
        for date in sorted([key for key in sp_500_date_tickers.keys()]):
            # import ipdb; ipdb.set_trace() # debugging starts here
            current_sp500 = sp_500_date_tickers[date]
            for ticker_removed in set(previous_sp500) - set(current_sp500):
                sp_500_ticker_periods[ticker_removed][-1].append(date)
            for ticker_added in set(current_sp500) - set(previous_sp500):
                if sp_500_ticker_periods.get(ticker_added):
                    sp_500_ticker_periods[ticker_added].append(
                        [
                            (
                                pd.to_datetime(date) + datetime.timedelta(days=1)
                            ).strftime("%Y-%m-%d")
                        ]
                    )
                else:
                    sp_500_ticker_periods[ticker_added] = [
                        [
                            (
                                pd.to_datetime(date) + datetime.timedelta(days=1)
                            ).strftime("%Y-%m-%d")
                        ]
                    ]
            previous_sp500 = sp_500_date_tickers[date]

        for value in sp_500_ticker_periods.values():
            if len(value[-1]) == 1:
                value[-1].append(self.end.strftime("%Y-%m-%d"))

        return sp_500_ticker_periods
