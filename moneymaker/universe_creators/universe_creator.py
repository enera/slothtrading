from abc import ABC, abstractmethod
from typing import List, Dict, Tuple
import datetime


class universe_creator(ABC):
    @abstractmethod
    def get_history(
        self,
    ) -> Dict[str, List[Tuple[datetime.date, datetime.date]]]:
        """
        Get the complete history of the universe.

        Returns:
            Dict[str, List[Tuple[datetime.date, datetime.date]]]: The keys of the dict
            represent the tickers of the universe. Each ticker has a list tuples which
            represent date ranges in which the ticker was part of the universe.
        """
        pass

    # @abstractmethod
    # def get_universe(self, date: Union[datetime.date, None] = None) -> List[str]:
    #     """
    #     Get all tickers that are part of the universe on the specified date. If no date
    #     is specified the most current date is chosen on which the universe is defined.

    #     Args:
    #         date (Union[datetime.date, None], optional): Date on which the tickers are
    #         part of the universe. Defaults to None.

    #     Returns:
    #         List[str]: Tickers of the universe.
    #     """
    #     pass
