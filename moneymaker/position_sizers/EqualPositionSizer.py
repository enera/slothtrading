import pandas as pd
from typing import List, Tuple


class EqualPositionSizer:
    # Percentage of account value
    def __init__(self, postion_size: float, allow_fractional=False):
        self.position_size = postion_size
        self.allow_fractional = allow_fractional

    def __call__(
        self,
        ticker_bar: pd.Series,
        acount_value: float,
    ) -> float:
        size = acount_value * self.position_size / ticker_bar["close"]
        if not self.allow_fractional:
            size = round(size)
        return size

    def rebalance(
        self,
        data: pd.DataFrame,
        positions: List[Tuple[str, float]],
        acount_value: float,
    ) -> List[Tuple[str, float]]:
        balance = []
        for ticker, size in positions:
            if ticker in data.index:
                new_size = self.__call__(data.loc[ticker, :], acount_value)
                balance.append((ticker, new_size - size))
        return balance
