import pandas as pd
from typing import List, Tuple


class ATRPositionSizer:
    # ATR * Shares = AcountValue * RiskFactor
    def __init__(self, risk_factor, allow_fractional=False):
        self.risk_factor = risk_factor
        self.allow_fractional = allow_fractional

    def __call__(
        self,
        ticker_bar: pd.Series,
        acount_value: float,
    ) -> float:
        size = acount_value * self.risk_factor / ticker_bar["atr"]
        if size * ticker_bar["close"] < acount_value * 0.05:
            size = acount_value * 0.05 / ticker_bar["close"]
        elif size * ticker_bar["close"] > acount_value * 0.15:
            size = acount_value * 0.15 / ticker_bar["close"]
        if not self.allow_fractional:
            size = round(size)
        return size

    def rebalance(
        self,
        data: pd.DataFrame,
        positions: List[Tuple[str, float]],
        acount_value: float,
    ) -> List[Tuple[str, float]]:
        balance = []
        for ticker, size in positions:
            if ticker in data.index:
                new_size = self.__call__(data.loc[ticker, :], acount_value)
                balance.append((ticker, new_size - size))
        return balance
