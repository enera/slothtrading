from enum import Enum
import requests

class Condition(Enum):
    INFO = "ℹ"
    WARNING = "⚠"
    DEBUG = "🤖"
    ERROR = "🔥"

class Logger:

    def __init__(self, token, chat_id):
        self.token = token
        self.chat_id = chat_id
        self.url = f"https://api.telegram.org/bot{token}"

    def log(self, message, condition=None):
        text = message
        if condition is not None:
            text = f"{condition.value}{text}"

        url = self.url +"/sendMessage"
        param = {"chat_id": self.chat_id,
                 "text": text}
        resp = requests.post(
            url=url,
            params=param
        )

        resp.raise_for_status()


    def warning(self, message):
        self.log(message, condition=Condition.WARNING)

    def info(self, message):
        self.log(message, condition=Condition.INFO)

    def debug(self, message):
        self.log(message, condition=Condition.DEBUG)

    def error(self, message):
        self.log(message, condition=Condition.ERROR)
