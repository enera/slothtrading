
cpdef enum instrument_type:
    type_cash = 0
    type_stock = 1
    type_option = 2
    type_crypto = 3

    # special instruments
    type_pending_order = 10
    type_taxes = 11
    type_balance_taxes = 12
    type_balance_margin = 13

cpdef enum instrument_unit:
    unit_no_unit = 0
    unit_euro = 1
    unit_dollar = 2

