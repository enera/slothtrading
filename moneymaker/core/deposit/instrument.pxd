from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.core cimport Core
from moneymaker.core.deposit.instrument_enum cimport *

# build sqlcon
# sqlcon: dbtablename deposit_instrument
# sqlcon: convert str VARCHAR(12)
# sqlcon: unsigned min_quantity max_quantity size_increment margin_rate_init margin_rate_maintaining price_increment

cdef class Instrument(Persist):

    cdef:
        instrument_type type
        instrument_unit unit
        str name
        str isin
        str ticker
        dict positions
        instrument_unit unit_price
        double price
        int min_quantity
        int max_quantity

        # the minimum price/size increment or tick size for the instrument
        double price_increment
        double size_increment


        double margin_rate_init
        double margin_rate_maintaining

    #-- PUBLIC METHODS ---------------------------------------------------------------
    # method to add postions to instrument by list of positions
    cdef void append_positions(self, dict new_positions)

    #-- Persistence -------------------------------------------------------------------
    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef Instrument load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef Instrument make(
        int id,
        Core core,
        instrument_type type,
        instrument_unit unit,
        instrument_unit unit_price,
        str ticker = *,
        str isin = *
    )

    @staticmethod
    cdef Instrument make_cash_instrument(
            int id,
            Core core,
            str name,
            instrument_unit unit
    )
    
    # method to link data ids with objects
    cdef void link(self)

    # print object information
    cpdef void print_obj(self)
