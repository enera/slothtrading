from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.deposit.position cimport Position
from moneymaker.core.deposit.transaction cimport Transaction
from moneymaker.core.deposit.instrument cimport Instrument
from moneymaker.core.deposit.instrument_enum cimport instrument_unit

from moneymaker.core.helper.timeint cimport get_time_ns
from libcpp cimport bool


cdef class Portfolio(Persist):

    def __init__(
        self,
        int id,
        Core core,
        str name,
        dict positions,
        Position position_cash,
        Position position_pending_orders,
        Position position_taxes,
        Position position_balance_taxes,
        Position position_balance_margin,
        int id_position_cash,
        int id_position_pending_orders,
        int id_position_taxes,
        int id_position_balance_taxes,
        int id_position_balance_margin,
        int time_last_changed,
        bool consider_taxes,
        double tax_rate
    ):
        super().__init__(id, core)
        self.name = name
        self.positions = positions
        self.position_cash = position_cash
        self.position_pending_orders = position_pending_orders
        self.position_taxes = position_taxes
        self.position_balance_taxes = position_balance_taxes
        self.position_balance_margin = position_balance_margin
        self.id_position_cash = id_position_cash
        self.id_position_pending_orders = id_position_pending_orders
        self.id_position_taxes = id_position_taxes
        self.id_position_balance_taxes = id_position_balance_taxes
        self.id_position_balance_margin = id_position_balance_margin
        self.time_last_changed = time_last_changed
        self.consider_taxes = consider_taxes
        self.tax_rate = tax_rate

        if core is not None and id > 0:
            core.portfolios[id] = self

    # method to create new object
    @staticmethod
    cdef Portfolio make(
        int id,
        Core core
    ):
        cdef str name = ""
        cdef dict positions = {}
        cdef int time_last_changed = get_time_ns()
        cdef bool consider_taxes = False
        cdef double tax_rate = 0.26375

        return Portfolio(
            id,
            core,
            name,
            positions,
            None,
            None,
            None,
            None,
            None,
            0,
            0,
            0,
            0,
            0,
            time_last_changed,
            consider_taxes,
            tax_rate
        )

    @staticmethod
    cdef Portfolio make_default_portfolio(
        Core core,
        instrument_unit init_cash_unit,
        double init_cash
    ):
        cdef int new_id = core.idmanager.ask_id_sync()
        cdef str name = "new_portfolio"
        cdef dict positions = {}
        cdef int time_last_changed = get_time_ns()
        cdef bool consider_taxes = False
        cdef double tax_rate = 0.26375

        return Portfolio(
            new_id,
            core,
            name,
            positions,
            None,
            None,
            None,
            None,
            0,
            0,
            0,
            0,
            0,
            time_last_changed,
            consider_taxes,
            tax_rate
        )
        


    # print object information
    cpdef void print_obj(self):
        print("Portfolio, ID ", str(self.id))


    cdef Position get_position_by_ticker(self, str ticker):
        cdef Instrument instr = self.core.get_instrument_by_ticker(ticker)

        if instr is None:
            return None

        return self.get_position_by_instrument(instr)

    cdef Position get_position_by_isin(self, str isin):
        cdef Instrument instr = self.core.get_instrument_by_isin(isin)
        if instr is None:
            return None

        return self.get_position_by_instrument(instr)

    cdef Position get_position_by_instrument(self, Instrument instrument):
        cdef Position pos
        cdef int i

        for i, pos in self.positions.items():
            if pos.instrument == instrument:
                return pos
        return None

    cdef Position get_position_by_id(self, int id):
        return self.positions.get(id)

    cdef Position get_position_pending_orders(self):
        return self.position_pending_orders

    cdef Position add_position(self, Instrument instrument, double amount = 0.0):
        cdef Position check_pos = self.get_position_by_instrument(instrument)
        if check_pos is not None:
            print(
                "Multiple positions of same instrument not allowed",
                "in same portfolio. Initial amount not considered.",
                "Returned existing position."
            )
            return check_pos

        return self.add_position_nocheck(instrument, amount)
        

    cdef Position add_position_nocheck(self, Instrument instrument, double amount = 0.0):
        cdef int id = self.core.idmanager.ask_id_sync()

        cdef Position new_position = Position.make(
            id, self.core, amount, instrument, self
        )

        self.positions[id] = new_position
        return new_position

    cdef double get_single_balance(
        self, 
        int id = 0, 
        str isin = "", 
        str ticker = ""
    ):
        """
        Get balance of single symbol.
        :param: id or isin or ticker
        :return: amount, cash
        """
        if all(arg is None for arg in {id, isin, ticker}):
            raise ValueError('Not enough arg to identify symbol')

        cdef Position pos_obj

        if id:
            pos_obj = self.core.get_position(id)

        elif isin:
            for i in range(0,len(self.l_position)):
                if isin == self.l_position[i].isin:
                    pos_obj = self.l_position[i]
        elif ticker:
            for i in range(0,len(self.l_position)):
                if ticker == self.l_position[i].ticker:
                    pos_obj = self.l_position[i]

        return pos_obj.get_balance()
    
    cdef double get_cash(self):
        """
        return cash of portfolio
        """
        cdef: 
            double cash
            int i
            Instrument inst

        for i in range(len(self.l_position)):
            pos_obj = self.l_position[i]
            if pos_obj.instrument.type == type_cash:
                cash = pos_obj.get_balance()
                return cash

    cdef double get_total_pf(self):
        cdef: 
            double total_value = 0
            int i
            Position pos_obj

        for i in range(0, len(self.l_position)):
            pos_obj = self.l_position[i]
            if pos_obj.instrument.type != type_cash:
                total_value += pos_obj.get_balance()

        return total_value



    cdef void process_order(self, Transaction transaction):
        """
        Update Instrument, Portfolio and Position by id
        """
        pass
        
    # autocreated sqlcon function block - do not change
    # last generator run at 2022-02-25 10:04:56

    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS deposit_portfolio("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "
            + "name VARCHAR(20), "
            + "id_position_cash INT, "
            + "id_position_pending_orders INT, "
            + "id_position_taxes INT, "
            + "id_position_balance_taxes INT, "
            + "id_position_balance_margin INT, "
            + "time_last_changed INT, "
            + "consider_taxes BOOL, "
            + "tax_rate DOUBLE)"
        )

        cursor.execute(sqlquery)

        sqlquery = (
            "INSERT INTO deposit_portfolio VALUES ("
            + str(self.id) + ", "
            + str(self.name) + ", "
            + str(self.id_position_cash) + ", "
            + str(self.id_position_pending_orders) + ", "
            + str(self.id_position_taxes) + ", "
            + str(self.id_position_balance_taxes) + ", "
            + str(self.id_position_balance_margin) + ", "
            + str(self.time_last_changed) + ", "
            + str(self.consider_taxes) + ", "
            + str(self.tax_rate) + ")"
        )

        cursor.execute(sqlquery)

        db.commit()


    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE deposit_portfolio SET "
            + "name = " + str(self.name) + ", "
            + "id_position_cash = " + str(self.id_position_cash) + ", "
            + "id_position_pending_orders = " + str(self.id_position_pending_orders) + ", "
            + "id_position_taxes = " + str(self.id_position_taxes) + ", "
            + "id_position_balance_taxes = " + str(self.id_position_balance_taxes) + ", "
            + "id_position_balance_margin = " + str(self.id_position_balance_margin) + ", "
            + "time_last_changed = " + str(self.time_last_changed) + ", "
            + "consider_taxes = " + str(self.consider_taxes) + ", "
            + "tax_rate = " + str(self.tax_rate) + " "
            + "WHERE id = " + str(self.id)
        )

        cursor.execute(sqlquery)

        db.commit()


    @staticmethod
    cdef Portfolio load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM deposit_portfolio" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        
        cdef str name = row[1]
        cdef int id_position_cash = <int>row[2]
        cdef int id_position_pending_orders = <int>row[3]
        cdef int id_position_taxes = <int>row[4]
        cdef int id_position_balance_taxes = <int>row[5]
        cdef int id_position_balance_margin = <int>row[6]
        cdef int time_last_changed = <int>row[7]
        cdef bool consider_taxes = <bool>row[8]
        cdef double tax_rate = <double>row[9]

        cdef dict positions = {}
        return Portfolio(
            id,
            core,
            name, 
            positions,
            None,
            None,
            None,
            None,
            None,
            id_position_cash, 
            id_position_pending_orders, 
            id_position_taxes, 
            id_position_balance_taxes, 
            id_position_balance_margin, 
            time_last_changed, 
            consider_taxes, 
            tax_rate
        )
    # autocreated sqlcon function block end - do not change
