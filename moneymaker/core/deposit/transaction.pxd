from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.deposit.position cimport Position
from moneymaker.core.core cimport Core
from moneymaker.core.deposit.portfolio cimport Portfolio
from moneymaker.core.deposit.instrument cimport Instrument
from moneymaker.core.helper.idmanager cimport IDManager
from libcpp cimport bool

# build sqlcon
# sqlcon: dbtablename deposit_transaction
# sqlcon: convert str VARCHAR(16)
# sqlcon: notnull from_id to_id time_send
# sqlcon: unsigned from_id to_id commissions_unit_id

cpdef enum booking_status:
    status_undefined = 0
    status_pending = 1
    status_fulfilled = 2
    status_cancelled = 3

cpdef enum transaction_sender:
    sender_undefined = 0
    sender_strategy = 1
    sender_broker = 2

cpdef enum booking_target:
    target_undefined = 0
    target_manual_action = 1
    target_interactive_brokers = 2

cpdef enum booking_ordertype:
    ordertype_undefined = 0

    # odertypes from strategy
    ordertype_market = 1
    ordertype_limit = 2
    ordertype_stop_market = 3
    ordertype_stop_limit = 4
    ordertype_opening_auction = 5

    # automatic ordertypes from broker
    ordertype_executed_stop_loss = 6
    ordertype_executed_take_profit = 7
    ordertype_executed_time_limit = 8
    ordertype_executed_margin_call = 9
    ordertype_executed_delisting = 10


cdef class Transaction(Persist):

    cdef:
        Position position_from
        Position position_to
        int id_from
        int id_to
        double amount_from
        double amount_to
        double amount_from_realized
        double amount_to_realized
        int time_send
        int time_realized
        int commissions_unit_id
        double commissions_realized
        booking_status status
        transaction_sender sender
        booking_target target
        booking_ordertype ordertype
        double order_limit_price
        bool use_stop_loss
        double stop_price
        bool use_take_profit
        double take_profit_price
        bool use_execution_datetime
        int execution_datetime
        int id_connected_transaction
        Transaction connected_transaction

    # update in case broker recieved and realized order
    cdef void update(
        self,
        double amount_from_realized, 
        double amount_to_realized, 
        double commissions_realized, 
        booking_status status
    )
    

    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef Transaction load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef Transaction make(        
        int id, 
        Core core,
        Position position_from, 
        Position position_to,
        double amount_from, 
        double amount_to,
        transaction_sender sender,
        booking_target target, 
        booking_ordertype ordertype,
        double order_limit_price
    )

    # find positions and make new default buy order
    @staticmethod
    cdef Transaction make_buy(
        IDManager idmanager,
        Core core,
        Portfolio portfolio,
        Instrument instrument_to,
        double amount_to,
        booking_target target, 
        booking_ordertype ordertype,
        double order_limit_price 
    )

    # find positions and make new default sell order
    @staticmethod
    cdef Transaction make_sell(
        IDManager idmanager,
        Core core,
        Portfolio portfolio,
        Instrument instrument_from,
        double amount_from,
        booking_target target, 
        booking_ordertype ordertype,
        double order_limit_price 
    )

    # ordertype for auto created orders from broker side
    # (e.g. stoploss triggered)
    @staticmethod
    cdef Transaction make_automatic_order_broker(
        IDManager idmanager,
        Transaction connected_transaction,
        double amount_from,
        double amount_to,
        int commissions_unit_id,
        double commissions_realized,
        booking_ordertype ordertype
    )

    cdef void add_stop_loss(self, double price)
    cdef void add_take_profit(self, double price)
    cdef void add_execution_datetime(self, int datetime)

    # add stop loss, take profit and time limit execution
    cdef void add_triple_barrier(
        self,
        double loss_price,
        double take_profit_price,
        int execution_datetime
    )

    # print class
    cpdef void print_obj(self)

    cdef void link(self)
