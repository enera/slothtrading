from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.deposit.instrument cimport Instrument
from moneymaker.core.deposit.instrument_enum cimport *
from moneymaker.core.deposit.position cimport Position
from moneymaker.core.core cimport Core
from moneymaker.core.deposit.transaction cimport Transaction
from libcpp cimport bool

# build sqlcon
# sqlcon: dbtablename deposit_portfolio
# sqlcon: convert str VARCHAR(20)


cdef class Portfolio(Persist):

    cdef:
        str name
        dict positions

        # special positions
        Position position_cash
        Position position_pending_orders
        Position position_taxes
        Position position_balance_taxes
        Position position_balance_margin

        int id_position_cash
        int id_position_pending_orders
        int id_position_taxes
        int id_position_balance_taxes
        int id_position_balance_margin

        int time_last_changed
        bool consider_taxes
        double tax_rate

    cdef Position get_position_by_ticker(self, str ticker)
    cdef Position get_position_by_isin(self, str isin)
    cdef Position get_position_by_instrument(self, Instrument instrument)
    cdef Position get_position_by_id(self, int id)
    cdef Position add_position(self, Instrument instrument, double amount = *)
    cdef Position add_position_nocheck(self, Instrument instrument, double amount = *)
    cdef void process_order(self, Transaction transaction)
    # accounting methods:
    cdef double get_single_balance(self, int id = *, str isin = *, str ticker = *)

    cdef double get_total_pf(self)
    cdef double get_cash(self)
    
    # return list of open positions
    cdef Position get_position_pending_orders(self)

    # Persistence 
    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef Portfolio load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef Portfolio make(
        int id,
        Core core
    )

    # method to create new portfolio w initial special positions
    @staticmethod
    cdef Portfolio make_default_portfolio(
        Core core,
        instrument_unit init_cash_unit,
        double init_cash
    )

    # print object information
    cpdef void print_obj(self)
