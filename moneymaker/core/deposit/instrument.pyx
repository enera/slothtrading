from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.core cimport Core
from moneymaker.core.helper.timeint cimport get_time_ns
from moneymaker.core.deposit.instrument_enum cimport *


cdef class Instrument(Persist):

    def __init__(
        self,
        int id,
        Core core,
        instrument_type type,
        instrument_unit unit,
        str name,
        str ticker,
        str isin,
        dict positions,
        instrument_unit unit_price,
        double price,
        int min_quantity,
        int max_quantity,
        double price_increment,
        double size_increment,
        double margin_rate_init,
        double margin_rate_maintaining
    ):
        super().__init__(id, core)
        self.type = type
        self.unit = unit
        self.name = name
        self.ticker = ticker
        self.isin = isin
        self.positions = positions
        self.unit_price = unit_price
        self.price = price
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.price_increment = price_increment
        self.size_increment = size_increment
        self.margin_rate_init = margin_rate_init
        self.margin_rate_maintaining = margin_rate_maintaining

        if core is not None and id > 0:
            core.instruments[id] = self


    # method to create new object
    @staticmethod
    cdef Instrument make(
        int id,
        Core core,
        instrument_type type,
        instrument_unit unit,
        instrument_unit unit_price,
        str ticker = "",
        str isin = ""


    ):

        cdef str name = ""
        cdef dict positions = {}
        cdef double price = 0.0
        cdef int min_quantity = 0
        cdef int max_quantity = 0
        cdef double price_increment = 0.0
        cdef double size_increment = 0.0
        cdef double margin_rate_init = 0.0
        cdef double margin_rate_maintaining = 0.0

        if not isin and not ticker: 
            raise ValueError("Isin or ticker must be given")
        
        return Instrument(
            id,
            core,
            type,
            unit,
            name,
            ticker,
            isin,
            positions,
            unit_price,
            price,
            min_quantity,
            max_quantity,
            price_increment,
            size_increment,
            margin_rate_init,
            margin_rate_maintaining
        )

    @staticmethod
    cdef Instrument make_cash_instrument(
            int id,
            Core core,
            str name,
            instrument_unit unit
    ):
        cdef str ticker = ""
        cdef str isin = ""
        cdef dict positions = dict()
        cdef unit_price = unit_no_unit
        cdef price = 0.0
        cdef int min_quantity = 0
        cdef int max_quantity = 0
        cdef double price_increment = 0.0
        cdef double size_increment = 0.0
        cdef double margin_rate_init = 0.0
        cdef double margin_rate_maintaining = 0.0

        return Instrument(
            id,
            core,
            type_cash,
            unit,
            name,
            ticker,
            isin,
            positions,
            unit_price,
            price,
            min_quantity,
            max_quantity,
            price_increment,
            size_increment,
            margin_rate_init,
            margin_rate_maintaining
        )


    # print object information
    cpdef void print_obj(self):
        print("Instrument, ID: ", str(self.id))

    cdef void append_positions(self, dict new_positions):
        """
        Method to append postions to instrument.
        :param: dict of position objects
        """
        cdef int i
        cdef int max_pos = len(new_positions)

        # assert that none of the ids are already inserted
        assert len(set(new_positions) & set(self.positions)) == 0

        self.positions.update(new_positions)
        self.time_last_changed = get_time_ns()
    
    cdef void link(self):
        pass

    # autocreated sqlcon function block - do not change
    # last generator run at 2022-02-25 10:04:56

    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS deposit_instrument("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "
            + "type SMALLINT UNSIGNED, "
            + "unit SMALLINT UNSIGNED, "
            + "name VARCHAR(12), "
            + "ticker VARCHAR(12), "
            + "isin VARCHAR(12), "
            + "unit_price SMALLINT UNSIGNED, "
            + "price DOUBLE, "
            + "min_quantity INT NOT NULL, "
            + "max_quantity INT NOT NULL, "
            + "price_increment DOUBLE NOT NULL, "
            + "size_increment DOUBLE NOT NULL, "
            + "margin_rate_init DOUBLE NOT NULL, "
            + "margin_rate_maintaining DOUBLE NOT NULL)"
        )

        cursor.execute(sqlquery)

        sqlquery = (
            "INSERT INTO deposit_instrument VALUES ("
            + str(self.id) + ", "
            + str(self.type) + ", "
            + str(self.unit) + ", "
            + str(self.name) + ", "
            + str(self.ticker) + ", "
            + str(self.isin) + ", "
            + str(self.unit_price) + ", "
            + str(self.price) + ", "
            + str(self.min_quantity) + ", "
            + str(self.max_quantity) + ", "
            + str(self.price_increment) + ", "
            + str(self.size_increment) + ", "
            + str(self.margin_rate_init) + ", "
            + str(self.margin_rate_maintaining) + ")"
        )

        cursor.execute(sqlquery)

        db.commit()


    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE deposit_instrument SET "
            + "type = " + str(self.type) + ", "
            + "unit = " + str(self.unit) + ", "
            + "name = " + str(self.name) + ", "
            + "ticker = " + str(self.ticker) + ", "
            + "isin = " + str(self.isin) + ", "
            + "unit_price = " + str(self.unit_price) + ", "
            + "price = " + str(self.price) + ", "
            + "min_quantity = " + str(self.min_quantity) + ", "
            + "max_quantity = " + str(self.max_quantity) + ", "
            + "price_increment = " + str(self.price_increment) + ", "
            + "size_increment = " + str(self.size_increment) + ", "
            + "margin_rate_init = " + str(self.margin_rate_init) + ", "
            + "margin_rate_maintaining = " + str(self.margin_rate_maintaining) + " "
            + "WHERE id = " + str(self.id)
        )

        cursor.execute(sqlquery)

        db.commit()


    @staticmethod
    cdef Instrument load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM deposit_instrument" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        
        cdef instrument_type type = <instrument_type>row[1]
        cdef instrument_unit unit = <instrument_unit>row[2]
        cdef str name = row[3]
        cdef str ticker = row[4]
        cdef str isin = row[5]
        cdef instrument_unit unit_price = <instrument_unit>row[6]
        cdef double price = <double>row[7]
        cdef int min_quantity = <int>row[8]
        cdef int max_quantity = <int>row[9]
        cdef double price_increment = <double>row[10]
        cdef double size_increment = <double>row[11]
        cdef double margin_rate_init = <double>row[12]
        cdef double margin_rate_maintaining = <double>row[13]

        cdef dict positions = {}
        return Instrument(
            id,
            core,
            type, 
            unit, 
            name,
            ticker,
            isin,
            positions,
            unit_price, 
            price, 
            min_quantity, 
            max_quantity, 
            price_increment, 
            size_increment, 
            margin_rate_init, 
            margin_rate_maintaining
        )
    # autocreated sqlcon function block end - do not change
