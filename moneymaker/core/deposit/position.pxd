from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.core cimport Core
from moneymaker.core.deposit.instrument cimport Instrument
from moneymaker.core.deposit.instrument_enum cimport *
from moneymaker.core.deposit.portfolio cimport Portfolio

# build sqlcon
# sqlcon: dbtablename deposit_position
# sqlcon: notnull from_id to_id time_created amount
# sqlcon: unsigned from_id to_id time_created time_last_update


cdef class Position(Persist):

    cdef:
        str name
        double amount
        Instrument instrument
        Portfolio portfolio
        int id_instrument
        int id_portfolio
        int time_created
        int time_last_update


    # method to save element in DB
    cdef void save_in_db(self, object db)

    # method to update element in DB
    cdef void update_in_db(self, object db)

    # method to load object data from DB
    @staticmethod
    cdef Position load_from_db(object db, int id, Core core)

    # method to create new object
    @staticmethod
    cdef Position make(
        int id,
        Core core,
        double amount,
        Instrument instrument,
        Portfolio portfolio
    )

    # print object information
    cpdef void print_obj(self)

    # method to link data ids with objects
    cdef void link(self)

    cdef void delta_update(self, double delta_amount)
    cdef void abs_update(self, double abs_amount)
    cdef double get_balance(self)
    cdef instrument_unit get_balance_unit(self)

