from moneymaker.core.helper.timeint cimport get_time_ns
from moneymaker.core.core cimport Core

cdef class Transaction(Persist):
    """
    Class for transactions between different instruments/ positions.
    Gets initialized with pending state.
    Update when fullfilled using the update function.

    """

    def __init__(
        self,
        int id,
        Core core,
        Position position_from,
        Position position_to,
        int id_from,
        int id_to,
        double amount_from,
        double amount_to,
        double amount_from_realized,
        double amount_to_realized,
        int time_send,
        int time_realized,
        int commissions_unit_id,
        double commissions_realized,
        booking_status status,
        transaction_sender sender,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price,
        bool use_stop_loss,
        double stop_price,
        bool use_take_profit,
        double take_profit_price,
        bool use_execution_datetime,
        int execution_datetime,
        int id_connected_transaction
    ):
        super().__init__(id, core)

        self.position_from = position_from
        self.position_to = position_to
        self.id_from = id_from
        self.id_to = id_to
        self.amount_from = amount_from
        self.amount_to = amount_to
        self.amount_from_realized = amount_from_realized
        self.amount_to_realized = amount_to_realized
        self.time_send = time_send
        self.time_realized = time_realized
        self.commissions_unit_id = commissions_unit_id
        self.commissions_realized = commissions_realized
        self.status = status
        self.sender = sender
        self.target = target
        self.ordertype = ordertype
        self.order_limit_price = order_limit_price
        self.use_stop_loss = use_stop_loss
        self.stop_price = stop_price
        self.use_take_profit = use_take_profit
        self.take_profit_price = take_profit_price
        self.use_execution_datetime = use_execution_datetime
        self.execution_datetime = execution_datetime
        self.id_connected_transaction = id_connected_transaction

        if core is not None and id > 0:
            core.transactions[id] = self

        #initialize
        #self.process_order()

    cdef void update(
        self,
        double amount_from_realized,
        double amount_to_realized,
        double commissions_realized,
        booking_status status
    ):
        self.amount_from_realized = amount_from_realized
        self.amount_to_realized = amount_to_realized
        self.commissions_realized = commissions_realized
        self.status = status
        self.time_realized = get_time_ns()

        #self.process_order()


    # autocreated sqlcon function block - do not change
    # last generator run at 2022-02-25 10:04:56

    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS deposit_transaction("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "
            + "id_from INT UNSIGNED NOT NULL, "
            + "id_to INT UNSIGNED NOT NULL, "
            + "amount_from DOUBLE, "
            + "amount_to DOUBLE, "
            + "amount_from_realized DOUBLE, "
            + "amount_to_realized DOUBLE, "
            + "time_send INT NOT NULL, "
            + "time_realized INT, "
            + "commissions_unit_id INT NOT NULL, "
            + "commissions_realized DOUBLE, "
            + "status SMALLINT UNSIGNED, "
            + "sender SMALLINT UNSIGNED, "
            + "target SMALLINT UNSIGNED, "
            + "ordertype SMALLINT UNSIGNED, "
            + "order_limit_price DOUBLE, "
            + "use_stop_loss BOOL, "
            + "stop_price DOUBLE, "
            + "use_take_profit BOOL, "
            + "take_profit_price DOUBLE, "
            + "use_execution_datetime BOOL, "
            + "execution_datetime INT, "
            + "id_connected_transaction INT)"
        )

        cursor.execute(sqlquery)

        sqlquery = (
            "INSERT INTO deposit_transaction VALUES ("
            + str(self.id) + ", "
            + str(self.id_from) + ", "
            + str(self.id_to) + ", "
            + str(self.amount_from) + ", "
            + str(self.amount_to) + ", "
            + str(self.amount_from_realized) + ", "
            + str(self.amount_to_realized) + ", "
            + str(self.time_send) + ", "
            + str(self.time_realized) + ", "
            + str(self.commissions_unit_id) + ", "
            + str(self.commissions_realized) + ", "
            + str(self.status) + ", "
            + str(self.sender) + ", "
            + str(self.target) + ", "
            + str(self.ordertype) + ", "
            + str(self.order_limit_price) + ", "
            + str(self.use_stop_loss) + ", "
            + str(self.stop_price) + ", "
            + str(self.use_take_profit) + ", "
            + str(self.take_profit_price) + ", "
            + str(self.use_execution_datetime) + ", "
            + str(self.execution_datetime) + ", "
            + str(self.id_connected_transaction) + ")"
        )

        cursor.execute(sqlquery)

        db.commit()


    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE deposit_transaction SET "
            + "id_from = " + str(self.id_from) + ", "
            + "id_to = " + str(self.id_to) + ", "
            + "amount_from = " + str(self.amount_from) + ", "
            + "amount_to = " + str(self.amount_to) + ", "
            + "amount_from_realized = " + str(self.amount_from_realized) + ", "
            + "amount_to_realized = " + str(self.amount_to_realized) + ", "
            + "time_send = " + str(self.time_send) + ", "
            + "time_realized = " + str(self.time_realized) + ", "
            + "commissions_unit_id = " + str(self.commissions_unit_id) + ", "
            + "commissions_realized = " + str(self.commissions_realized) + ", "
            + "status = " + str(self.status) + ", "
            + "sender = " + str(self.sender) + ", "
            + "target = " + str(self.target) + ", "
            + "ordertype = " + str(self.ordertype) + ", "
            + "order_limit_price = " + str(self.order_limit_price) + ", "
            + "use_stop_loss = " + str(self.use_stop_loss) + ", "
            + "stop_price = " + str(self.stop_price) + ", "
            + "use_take_profit = " + str(self.use_take_profit) + ", "
            + "take_profit_price = " + str(self.take_profit_price) + ", "
            + "use_execution_datetime = " + str(self.use_execution_datetime) + ", "
            + "execution_datetime = " + str(self.execution_datetime) + ", "
            + "id_connected_transaction = " + str(self.id_connected_transaction) + " "
            + "WHERE id = " + str(self.id)
        )

        cursor.execute(sqlquery)

        db.commit()


    @staticmethod
    cdef Transaction load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM deposit_transaction" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        
        cdef int id_from = <int>row[1]
        cdef int id_to = <int>row[2]
        cdef double amount_from = <double>row[3]
        cdef double amount_to = <double>row[4]
        cdef double amount_from_realized = <double>row[5]
        cdef double amount_to_realized = <double>row[6]
        cdef int time_send = <int>row[7]
        cdef int time_realized = <int>row[8]
        cdef int commissions_unit_id = <int>row[9]
        cdef double commissions_realized = <double>row[10]
        cdef booking_status status = <booking_status>row[11]
        cdef transaction_sender sender = <transaction_sender>row[12]
        cdef booking_target target = <booking_target>row[13]
        cdef booking_ordertype ordertype = <booking_ordertype>row[14]
        cdef double order_limit_price = <double>row[15]
        cdef bool use_stop_loss = <bool>row[16]
        cdef double stop_price = <double>row[17]
        cdef bool use_take_profit = <bool>row[18]
        cdef double take_profit_price = <double>row[19]
        cdef bool use_execution_datetime = <bool>row[20]
        cdef int execution_datetime = <int>row[21]
        cdef int id_connected_transaction = <int>row[22]

        return Transaction(
            id,
            core,
            None,
            None,
            id_from, 
            id_to, 
            amount_from, 
            amount_to, 
            amount_from_realized, 
            amount_to_realized, 
            time_send, 
            time_realized, 
            commissions_unit_id, 
            commissions_realized, 
            status, 
            sender, 
            target, 
            ordertype, 
            order_limit_price, 
            use_stop_loss, 
            stop_price, 
            use_take_profit, 
            take_profit_price, 
            use_execution_datetime, 
            execution_datetime, 
            id_connected_transaction
        )
    # autocreated sqlcon function block end - do not change

    cpdef void print_obj(self):
        print("Transaction: ")
        print("ID: ", self.id)
        print("FromID :", self.id_from)
        print("ToID :", self.id_to)
        print("\n")


    @staticmethod
    cdef Transaction make(
        int id,
        Core core,
        Position position_from,
        Position position_to,
        double amount_from,
        double amount_to,
        transaction_sender sender,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    ):

        # default params
        cdef int id_from = 0
        cdef int id_to = 0
        cdef double amount_from_realized = 0.0
        cdef double amount_to_realized = 0.0
        cdef int time_send = get_time_ns()
        cdef int time_realized = 0
        cdef int commissions_unit_id = 0
        cdef double commissions_realized = 0.0
        cdef booking_status status = status_pending
        cdef bool use_stop_loss = False
        cdef double stop_price = 0.0
        cdef bool use_take_profit = False
        cdef double take_profit_price = 0.0
        cdef bool use_execution_datetime = False
        cdef int execution_datetime = 0
        cdef int id_connected_transaction = 0

        return Transaction(
            id,
            core,
            position_from,
            position_to,
            id_from,
            id_to,
            amount_from,
            amount_to,
            amount_from_realized,
            amount_to_realized,
            time_send,
            time_realized,
            commissions_unit_id,
            commissions_realized,
            status,
            sender,
            target,
            ordertype,
            order_limit_price,
            use_stop_loss,
            stop_price,
            use_take_profit,
            take_profit_price,
            use_execution_datetime,
            execution_datetime,
            id_connected_transaction
        )

    @staticmethod
    cdef Transaction make_buy(
        IDManager idmanager,
        Core core,
        Portfolio portfolio,
        Instrument instrument_to,
        double amount_to,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    ):
        # check if position exists

        if portfolio.position_cash is None:
            print("Portfolio has no cash position")
            return None

        if ordertype == ordertype_limit:
            assert order_limit_price is not None
            assert order_limit_price > 0.0

        cdef Transaction pos_from
        cdef Transaction pos_to

        pos_from = portfolio.position_cash
        pos_to = portfolio.get_position_by_instrument(instrument_to)

        if pos_to is None:
            pos_to = portfolio.add_position_nocheck(instrument_to)

        cdef int new_id = idmanager.ask_id_sync()
        cdef double costs_estimate = 0.0

        if ordertype == ordertype_market:
            # todo check price unit and convert
            costs_estimate = instrument_to.price * amount_to
        if ordertype == ordertype_limit:
            costs_estimate = order_limit_price * amount_to

        cdef transaction_sender sender = sender_strategy

        return Transaction.make(
            new_id,
            core,
            pos_from,
            pos_to,
            costs_estimate,
            amount_to,
            sender,
            target,
            ordertype,
            order_limit_price
        )

    @staticmethod
    cdef Transaction make_sell(
        IDManager idmanager,
        Core core,
        Portfolio portfolio,
        Instrument instrument_from,
        double amount_from,
        booking_target target,
        booking_ordertype ordertype,
        double order_limit_price
    ):
        # check if position exists

        if portfolio.position_cash is None:
            print("Portfolio has no cash position")
            return None

        if ordertype == ordertype_limit:
            assert order_limit_price is not None
            assert order_limit_price > 0.0

        cdef Transaction pos_from
        cdef Transaction pos_to

        pos_to = portfolio.position_cash
        pos_from = portfolio.get_position_by_instrument(instrument_from)

        if pos_from is None:
            pos_from = portfolio.add_position_nocheck(instrument_from)

        cdef int new_id = idmanager.ask_id_sync()
        cdef double costs_estimate = 0.0

        if ordertype == ordertype_market:
            # todo check price unit and convert
            costs_estimate = instrument_from.price * amount_from
        if ordertype == ordertype_limit:
            costs_estimate = order_limit_price * amount_from

        cdef transaction_sender sender = sender_strategy

        return Transaction.make(
            new_id,
            core,
            pos_from,
            pos_to,
            amount_from,
            costs_estimate,
            sender,
            target,
            ordertype,
            order_limit_price
        )


    @staticmethod
    cdef Transaction make_automatic_order_broker(
        IDManager idmanager,
        Transaction connected_transaction,
        double amount_from,
        double amount_to,
        int commissions_unit_id,
        double commissions_realized,
        booking_ordertype ordertype
    ):
        cdef int new_id = idmanager.ask_id_sync()
        cdef Position pos_from = connected_transaction.position_to
        cdef Position pos_to = connected_transaction.position_from
        cdef int time_send = get_time_ns()
        cdef transaction_sender sender = sender_broker
        cdef booking_target target = connected_transaction.target
        cdef double order_limit_price = 0.0

        cdef Transaction new_transaction = Transaction.make(
            new_id,
            connected_transaction.core,
            pos_from,
            pos_to,
            amount_from,
            amount_to,
            sender,
            target,
            ordertype,
            order_limit_price
        )

        new_transaction.commissions_unit_id = commissions_unit_id
        new_transaction.commissions_realized = commissions_realized
        new_transaction.connected_id = connected_transaction.id
        new_transaction.id_from = connected_transaction.id_to
        new_transaction.id_to = connected_transaction.id_from
        new_transaction.target = connected_transaction.target

        return new_transaction

    cdef void add_stop_loss(self, double price):
        self.use_stop_loss = True
        self.stop_price = price


    cdef void add_take_profit(self, double price):
        self.use_take_profit = True
        self.take_profit_price = price


    cdef void add_execution_datetime(self, int time):
        self.use_execution_datetime = True
        self.execution_datetime = time


    cdef void add_triple_barrier(
            self,
            double loss_price,
            double take_profit_price,
            int execution_time
    ):
        self.add_stop_loss(loss_price)
        self.add_take_profit(take_profit_price)
        self.add_execution_datetime(execution_time)


    cdef void link(self):

        if (
            (self.id_from == 0 and self.position_from is None) or
           (self.id_to == 0 and self.position_to is None)
        ):
            raise RuntimeError("ID and Position null")

        if self.position_from is None:
            self.position_from = self.core.get_position(self.id_from)
        elif self.id_from == 0:
            self.id_from = self.position_from.id

        if self.position_to is None:
            self.position_to = self.core.get_position(self.id_to)
        elif self.id_to == 0:
            self.id_to = self.position_to.id
