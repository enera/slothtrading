from moneymaker.core.persistence.persist cimport Persist
from moneymaker.core.core cimport Core
from moneymaker.core.deposit.instrument cimport Instrument
from moneymaker.core.deposit.instrument_enum cimport *
from moneymaker.core.deposit.portfolio cimport Portfolio
from moneymaker.core.helper.timeint cimport get_time_ns


cdef class Position(Persist):

    def __init__(
        self,
        int id,
        Core core,
        str name,
        double amount,
        Instrument instrument,
        Portfolio portfolio,
        int id_instrument,
        int id_portfolio,
        int time_created,
        int time_last_update
    ):
        super().__init__(id, core)

        self.name = name
        self.amount = amount
        self.instrument = instrument
        self.portfolio = portfolio
        self.id_instrument = id_instrument
        self.id_portfolio = id_portfolio
        self.time_created = time_created
        self.time_last_update = time_last_update

        if core is not None and id > 0:
            core.positions[id] = self

    # method to create new object
    @staticmethod
    cdef Position make(
        int id,
        Core core,
        double amount,
        Instrument instrument,
        Portfolio portfolio
    ):
        cdef str name = ""
        cdef int id_instrument = 0
        cdef int id_portfolio = 0
        cdef int time_created = get_time_ns()
        cdef int time_last_update = 0

        return Position(
            id,
            core,
            name,
            amount,
            instrument,
            portfolio,
            id_instrument,
            id_portfolio,
            time_created,
            time_last_update
        )

    # print object information
    cpdef void print_obj(self):
        print("Position, ID: ", str(self.id))


    # method to link data ids with objects
    cdef void link(self):
        # # link to instrument, geht das so ueberhaupt und macht das sinN?
        # cdef dict new_position
        # if self.name:
        #     new_position = dict([(self.name, self)])
        # else:
        #     new_position = dict([(str(self.id), self)])
        # self.instrument.append_positions(new_position)
        pass

    # autocreated sqlcon function block - do not change
    # last generator run at 2022-02-25 10:04:56

    cdef void save_in_db(self, object db):
        
        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "CREATE TABLE IF NOT EXISTS deposit_position("
            # columns
            + "id INT UNSIGNED PRIMARY KEY, "
            + "name VARCHAR(24), "
            + "amount DOUBLE NOT NULL, "
            + "id_instrument INT, "
            + "id_portfolio INT, "
            + "time_created INT UNSIGNED NOT NULL, "
            + "time_last_update INT NOT NULL)"
        )

        cursor.execute(sqlquery)

        sqlquery = (
            "INSERT INTO deposit_position VALUES ("
            + str(self.id) + ", "
            + str(self.name) + ", "
            + str(self.amount) + ", "
            + str(self.id_instrument) + ", "
            + str(self.id_portfolio) + ", "
            + str(self.time_created) + ", "
            + str(self.time_last_update) + ")"
        )

        cursor.execute(sqlquery)

        db.commit()


    cdef void update_in_db(self, object db):
        
        cursor = db.cursor()

        cdef str sqlquery = (
            "UPDATE deposit_position SET "
            + "name = " + str(self.name) + ", "
            + "amount = " + str(self.amount) + ", "
            + "id_instrument = " + str(self.id_instrument) + ", "
            + "id_portfolio = " + str(self.id_portfolio) + ", "
            + "time_created = " + str(self.time_created) + ", "
            + "time_last_update = " + str(self.time_last_update) + " "
            + "WHERE id = " + str(self.id)
        )

        cursor.execute(sqlquery)

        db.commit()


    @staticmethod
    cdef Position load_from_db(object db, int id, Core core):

        cdef object cursor = db.cursor()

        cdef str sqlquery = (
            "SELECT * FROM deposit_position" +
            " WHERE id = " + str(id)
        )

        cursor.execute(sqlquery)

        cdef object res = cursor.fetchall()
        # todo: check if res len is == 0 and return empty or default object
        cdef object row = res[0]
        
        cdef str name = row[1]
        cdef double amount = <double>row[2]
        cdef int id_instrument = <int>row[3]
        cdef int id_portfolio = <int>row[4]
        cdef int time_created = <int>row[5]
        cdef int time_last_update = <int>row[6]

        return Position(
            id,
            core,
            name, 
            amount, 
            id_instrument, 
            id_portfolio, 
            time_created, 
            time_last_update
        )
    # autocreated sqlcon function block end - do not change


    cdef void delta_update(self, double delta_amount):
        assert self.amount + delta_amount > 0 

        self.amount = self.amount + delta_amount
        self.time_last_update = get_time_ns()


    cdef void abs_update(self, double abs_amount):
        assert abs_amount > 0

        self.amount = abs_amount
        self.time_last_update = get_time_ns()


    cdef double get_balance(self):
        cdef double value
        if not self.instrument.type == type_cash:
            value = self.instrument.price * self.amount
        else:
            value = self.amount
        return value

    cdef instrument_unit get_balance_unit(self):
        return self.instrument.unit_price