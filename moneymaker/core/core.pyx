from moneymaker.core.helper.sqlconnection cimport SQLConnection
cimport numpy as cnp
from moneymaker.core.deposit.instrument cimport *
from moneymaker.core.helper.idmanager cimport IDManager
cnp.import_array()

cdef class Core:

    def __init__(self, str dbname = "mmcore"):
        self.con = SQLConnection()
        self.con.connect()
        self.con.select_db(dbname)
        self.idmanager = IDManager(None, self.con)
        self.persistence_objects = dict()
        self.portfolios = dict()
        self.instruments = dict()
        self.transactions = dict()
        self.positions = dict()

        self.instrument_cash = None
        self.instrument_pending_orders = None
        self.instrument_taxes = None
        self.instrument_balance_taxes = None
        self.instrument_balance_margin = None


    cdef cnp.ndarray get_ids(self, str tablename):
        cdef str sqlquery = f"SELECT id FROM {tablename}"
        self.con.cursor_obj.execute(sqlquery)
        cdef object res = self.con.cursor_obj.fetchall()
        cdef cnp.npy_intp len_res = len(res)
        cdef cnp.ndarray arr = cnp.PyArray_SimpleNew(1, &len_res, cnp.NPY_INT32)
        cdef int i
        for i in range(len_res):
            arr[i] = res[i]
        return arr


    cdef void load_dataset(self, str dbname):
        self.load_deposit()
        self.load_strategy()


    cdef void load_deposit(self):

        # load ids
        cdef cnp.ndarray portfolio_ids = self.get_ids("deposit_portfolio")
        cdef cnp.ndarray instrument_ids = self.get_ids("deposit_instruments")
        cdef cnp.ndarray position_ids = self.get_ids("deposit_position")
        cdef cnp.ndarray transaction_ids = self.get_ids("deposit_transaction")

        # make objects
        cdef object db = self.con.db_obj
        cdef int i
        cdef int id

        for i in range(len(portfolio_ids)):
            id = portfolio_ids[i]
            Portfolio.load_from_db(db, id, self)
        
        for i in range(len(instrument_ids)):
            id = instrument_ids[i]
            Instrument.load_from_db(db, id, self)

        for i in range(len(position_ids)):
            id = position_ids[i]
            Position.load_from_db(db, id, self)
        
        for i in range(len(transaction_ids)):
            id = transaction_ids[i]
            Transaction.load_from_db(db, id, self)
        
        # make combined dict
        cdef dict d
        for d in [
            self.portfolios, 
            self.instruments, 
            self.positions, 
            self.transactions
        ]:
            self.persistence_objects.update(d)

        # link objects
        cdef object obj

        for id, obj in self.persistence_objects.items():
            obj.link()


    cdef void load_strategy(self):
        pass


    cdef void reset_special_instruments(self):
        self.instrument_cash = None
        self.instrument_pending_orders = None
        self.instrument_balance_taxes = None
        self.instrument_taxes = None
        self.instrument_balance_margin = None


    cdef void set_special_instruments_from_instruments_dict(self):
        self.reset_special_instruments()

        cdef Instrument inst
        cdef int id
        cdef int count = 0

        for id, inst in self.instruments.items():

            if inst.type == type_cash:
                self.instrument_cash = inst
                count += 1
            if inst.type == type_pending_order:
                self.instrument_pending_orders = inst
                count += 1
            elif inst.type == type_taxes:
                self.instrument_taxes = inst
                count += 1
            elif inst.type == type_balance_taxes:
                self.instrument_balance_taxes = inst
                count += 1
            elif inst.type == type_balance_margin:
                self.instrument_balance_margin = inst
                count += 1

            if count == 5:
                return


    cdef void make_special_instruments(self, instrument_unit cash_unit):

        cdef str cash_unit_str = ""
        if cash_unit == unit_euro:
            cash_unit_str = "euro"
        elif cash_unit == unit_dollar:
            cash_unit_str = "dollar"

        cdef Instrument inst_cash = Instrument.make_cash_instrument(
            self.idmanager.ask_id_sync(),
            self,
            "cash position " + cash_unit_str,
            cash_unit
        )
        inst_cash.type = type_cash
        self.instrument_cash = inst_cash

        cdef Instrument inst_pending_orders = Instrument.make_cash_instrument(
            self.idmanager.ask_id_sync(),
            self,
            "pending orders " + cash_unit_str,
            cash_unit
        )
        inst_cash.type = type_pending_order
        self.instrument_pending_orders = inst_pending_orders

        cdef Instrument inst_taxes = Instrument.make_cash_instrument(
            self.idmanager.ask_id_sync(),
            self,
            "taxes " + cash_unit_str,
            cash_unit
        )
        inst_taxes.type = type_taxes
        self.instrument_taxes = inst_taxes

        cdef Instrument inst_balance_taxes = Instrument.make_cash_instrument(
            self.idmanager.ask_id_sync(),
            self,
            "balance taxes " + cash_unit_str,
            cash_unit
        )
        inst_taxes.type = type_balance_taxes
        self.instrument_balance_taxes = inst_balance_taxes

        cdef Instrument inst_balance_margin = Instrument.make_cash_instrument(
            self.idmanager.ask_id_sync(),
            self,
            "balance margin " + cash_unit_str,
            cash_unit
        )
        inst_taxes.type = type_balance_margin
        self.instrument_balance_margin = inst_balance_margin



    cdef Portfolio get_portfolio(self, int id):
        return self.portfolios.get(id)


    cdef Position get_position(self, int id):
        return self.positions.get(id)


    cdef Instrument get_instrument(self, int id):
        return self.instruments.get(id)


    cdef Transaction get_transaction(self, int id):
        return self.transactions.get(id)


    cdef Instrument get_instrument_by_ticker(self, str ticker):
        cdef Instrument instr
        cdef int i

        for i, instr in self.instruments.items():
            if ticker == instr.ticker:
                return instr
        return None


    cdef Instrument get_instrument_by_isin(self, str isin):
        cdef Instrument instr
        cdef int i

        for i, instr in self.instruments.items():
            if instr.isin == isin:
                return instr
        return None