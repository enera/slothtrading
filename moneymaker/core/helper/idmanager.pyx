import asyncio
from moneymaker.core.helper.sqlconnection cimport SQLConnection
from libcpp cimport bool


cdef class IDManager:

    def __init__(
        self, 
        object q,
        SQLConnection con,
        int start_id = 100
        ):
        self.q = q
        self.con = con

        if q is not None:
            self.workertask = asyncio.create_task(self.worker(q))
        else:
            self.workertask = None

        # check if db is selected
        if not con.current_db:
            raise RuntimeError("SQL Connection: No DB initialized")

        cdef str sqlquery
        cdef object res
        cdef object row

        if con.table_exists("helper_idmanager"):

            sqlquery = (
                "SELECT * FROM helper_idmanager" +
                " WHERE id = 0"
            )

            self.con.cursor_obj.execute(sqlquery)

            res = self.con.cursor_obj.fetchall()

            if len(res) > 0:
                row = res[0]
                self.current_id = <int>row[1]
            else:
                sqlquery = (
                    "INSERT INTO helper_idmanager VALUES(" + 
                    f"0, {start_id})"
                )
                self.con.cursor_obj.execute(sqlquery)
                self.con.db_obj.commit()
                self.current_id = start_id

        else:
            sqlquery = (
                "CREATE TABLE helper_idmanager(" +
                    "id INT UNSIGNED PRIMARY KEY, counter INT UNSIGNED)"
                )

            con.cursor_obj.execute(sqlquery)

            sqlquery = (
                "INSERT INTO helper_idmanager VALUES(" + 
                f"0, {start_id})"
            )
            
            self.con.cursor_obj.execute(sqlquery)
            self.con.db_obj.commit()

            self.current_id = start_id

        self.current_id_dbsave = self.current_id

            
    async def worker(self, q: asyncio.Queue):

        cdef str sqlquery

        while True:
            await self.q.get()
            self.current_id += 1
            self.update_in_db()
            self.q.task_done()

    async def consumer_ask_id(self) -> int:

        assert self.q is not None

        while True:
            if self.q.full():
                await asyncio.sleep(0)
                continue

            await self.q.put(42)
            await self.q.join()
            return self.current_id

    async def cancel(self):
        if self.workertask is None:
            return
        self.workertask.cancel()
        await asyncio.gather(self.workertask, return_exceptions=True)
        self.workertask = None

    cdef int ask_id_sync(self, bool update_db = False):
        self.current_id += 1
        cdef str sqlquery

        if update_db:
           self.update_in_db()

        return self.current_id
        
    cdef void update_in_db(self):
        if self.current_id_dbsave == self.current_id:
            return

        sqlquery = (
            "UPDATE helper_idmanager SET "
            + "counter = " + str(self.current_id) 
            + " WHERE id = 0" 
        )
        self.con.cursor_obj.execute(sqlquery)
        self.con.db_obj.commit()
        self.current_id_dbsave = self.current_id
