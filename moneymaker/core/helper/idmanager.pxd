from moneymaker.core.helper.sqlconnection cimport SQLConnection
from libcpp cimport bool


cdef class IDManager:
    cdef:
        int current_id
        int current_id_dbsave
        object q
        object workertask
        SQLConnection con

    # ask for id synchronously 
    # (do not use both sync and async versions at same time)
    cdef int ask_id_sync(self, bool update_db = *)
    cdef void update_in_db(self)

