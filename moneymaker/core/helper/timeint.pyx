from libc.time cimport time


cdef int get_time_ns():
    return time(NULL)