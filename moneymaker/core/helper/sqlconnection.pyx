import json
import mysql.connector
from libcpp cimport bool
from moneymaker.definitions cimport get_root_dir


cdef class SQLConnection:
    
    def __init__(self):
        # imports file sqlconnection.json, 
        # please adapt template file sqlconnection_template.json
        fileopen = open(get_root_dir() + "/core/helper/sqlconnection.json")
        fileread = fileopen.read()
        cdef dict data = json.loads(fileread)

        if (
            "user" not in data
            or "password" not in data
            or "host" not in data
            or "port" not in data
        ):
            raise ValueError(
                "Incomplete SQL Connection Information. Check sqlconnection.json "
            )

        self.connection_info = data
        self.current_db = None
        self.cursor_obj = None
        self.db_obj = None

    cdef void connect(self):
        self.db_obj = mysql.connector.connect(
            host = self.connection_info["host"],
            user = self.connection_info["user"],
            password = self.connection_info["password"],
            port = self.connection_info["port"],
        )
        self.cursor_obj = self.db_obj.cursor()


    cdef void disconnect(self):
        self.cursor_obj.close()
        self.db_obj.close()
        self.cursor_obj = None
        self.db_obj = None
        self.current_db = None


    cdef void select_db(self, str db_name):
        self.cursor_obj.execute(f"CREATE DATABASE IF NOT EXISTS {db_name}")
        self.cursor_obj.execute(f"USE {db_name}")
        self.current_db = db_name


    cdef void drop_db(self, str db_name):
        self.cursor_obj.execute(f"DROP DATABASE IF EXISTS {db_name}")
        if self.current_db == db_name:
            self.current_db = None


    cdef void select_new_db(self, str db_name):
        self.drop_db(db_name)
        self.select_db(db_name)


    cdef bool table_exists(self, str table_name):
        self.cursor_obj.execute(f"SHOW TABLES LIKE '{table_name}'")
        cdef res = self.cursor_obj.fetchall()
        return len(res) > 0