from libcpp cimport bool

cdef class SQLConnection:
    
    cdef: 
        dict connection_info
        object db_obj
        object cursor_obj
        str current_db

    cdef void connect(self)
    cdef void disconnect(self)
    cdef void select_db(self, str db_name)
    cdef void drop_db(self, str db_name)
    cdef void select_new_db(self, str db_name)
    cdef bool table_exists(self, str table_name)

