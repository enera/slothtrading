cdef class Core:
    pass


cdef class Persist:

    cdef:
        int id
        Core core
        
    # abstract method to save element in DB
    cdef void save_in_db(self, object db)

    # abstract method to update element in DB
    cdef void update_in_db(self, object db)
    
    # abstract method to load object data from DB
    #@staticmethod
    #cdef Persist load_from_db(object db, int id, Core core)

    # method to link data based on ids (implement in subsclass)
    cdef void link(self)
