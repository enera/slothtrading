from moneymaker.core.core cimport Core
from moneymaker.core.core_impl cimport CoreImpl

cdef class Persist:

    def __init__(self, int element_id, Core core):
        self.id = element_id
        self.core = core

        if self.core is not None and self.id > 0:
            self.core.persistence_objects[id] = self

    # abstract method to save element in DB
    cdef void save_in_db(self, object db):
        """Abstract method (implement in subclass)."""
        raise NotImplementedError("method must be implemented in the subclass")


    # abstract method to load object data from DB
    cdef void update_in_db(self, object db):
        """Abstract method (implement in subclass)."""
        raise NotImplementedError("method must be implemented in the subclass")  


    # abstract method to load object data from DB
    #@staticmethod
    #cdef Persist load_from_db(object db, int id, Core core):
    #    """Abstract method (implement in subclass)."""
    #    raise NotImplementedError("method must be implemented in the subclass")  

    # method to link data based on ids
    cdef void link(self):
        pass
