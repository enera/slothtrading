from moneymaker.core.helper.sqlconnection cimport SQLConnection
from moneymaker.core.deposit.instrument cimport *
from moneymaker.core.deposit.portfolio cimport Portfolio
from moneymaker.core.deposit.transaction cimport Transaction
from moneymaker.core.deposit.position cimport Position
from moneymaker.core.helper.idmanager cimport IDManager
cimport numpy as cnp

cdef class Core:
    cdef:
        SQLConnection con
        IDManager idmanager
        dict persistence_objects
        dict portfolios
        dict instruments
        dict transactions
        dict positions

        # special instruments
        Instrument instrument_cash
        Instrument instrument_pending_orders
        Instrument instrument_taxes
        Instrument instrument_balance_taxes
        Instrument instrument_balance_margin

    cdef cnp.ndarray get_ids(self, str tablename)
    cdef void load_dataset(self, str dbname)
    cdef void load_deposit(self)
    cdef void load_strategy(self)

    cdef void reset_special_instruments(self)
    cdef void set_special_instruments_from_instruments_dict(self)
    cdef void make_special_instruments(self, instrument_unit cash_unit)

    cdef Portfolio get_portfolio(self, int id)
    cdef Position get_position(self, int id)
    cdef Instrument get_instrument(self, int id)
    cdef Transaction get_transaction(self, int id)

    cdef Instrument get_instrument_by_ticker(self, str ticker)
    cdef Instrument get_instrument_by_isin(self, str isin)
    