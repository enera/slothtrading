#include "core2/deposit/position.h"
#include "core2/deposit/instrument.h"
#include "core2/deposit/portfolio.h"
#include "core2/core.h"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("position")
{
    Core c("mmcore_test", true);
    auto &idmanager = c.get_id_manager();

    Instrument * i = Instrument::make(
        idmanager.ask_id(),
        &c,
        instrument_type::stock,
        instrument_unit::no_unit,
        instrument_unit::dollar, "AAPL", "US0378331005", "Apple"
    );

    constexpr double price = 23.55;
    i->set_price(price);

    Portfolio * pf = Portfolio::make(idmanager.ask_id(), &c);

    Position * pos = Position::make(idmanager.ask_id(), &c, 0.0, i, pf);
    pos->link();

    // check linking
    CHECK(pos->get_portfolio() == pf);
    CHECK(pos->get_instrument() == i);
    CHECK(pos->get_id_portfolio() == pf->get_id());
    CHECK(pos->get_id_instrument() == i->get_id());

    // check amount functions
    CHECK(pos->get_amount() == 0.0);
    pos->set_amount(5.0);
    CHECK(pos->get_amount() == 5.0);
    pos->delta_update(3.0);
    CHECK(pos->get_amount() == Approx(8.0));
    pos->delta_update(-2.0);
    CHECK(pos->get_amount() == Approx(6.0));
    pos->abs_update(3.0);
    CHECK(pos->get_amount() == 3.0);

    // check balance calculation
    CHECK(pos->get_balance() == price * pos->get_amount());


    // make cash position
    Instrument * cash_instr = Instrument::make_cash_instrument(
        idmanager.ask_id(),
        &c,
        "dollar",
        instrument_unit::dollar
    );

    Position * cash_pos = Position::make(idmanager.ask_id(), &c, 0.0, cash_instr, pf);
    cash_pos->link();
    cash_pos->delta_update(500.0);
    CHECK(cash_pos->get_amount() == 500.0);
    CHECK(cash_pos->get_balance() == 500.0);

}

