#include "core2/deposit/instrument.h"
#include "core2/deposit/position.h"
#include "core2/persistence/deposit/instrument_persist.h"


#include "core2/core.h"
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("instrument")
{
    Core c("mmcore_test", true);
    auto &idmanager = c.get_id_manager();

    Instrument * i = Instrument::make(
        idmanager.ask_id(), 
        &c, 
        instrument_type::stock,
        instrument_unit::no_unit, 
        instrument_unit::dollar, "AAPL", "US0378331005", "Apple"
    );

    CHECK(c.get_instrument_by_isin("US0378331005") != nullptr);

    i->set_price(300.0);

    Position * p1 = Position::make(idmanager.ask_id(), &c, 0.0, i, nullptr);
    Position * p2 = Position::make(idmanager.ask_id(), &c, 10.0, i, nullptr);
    Position * p3 = Position::make(idmanager.ask_id(), &c, 0.0, i, nullptr);

    i->append_position(p1);
    i->append_positions({ p2, p3});
    CHECK(i->get_positions().size() == 3);

    CHECK(i->get_core() == &c);
    CHECK(i->get_type() == instrument_type::stock);
    CHECK(i->get_unit() == instrument_unit::no_unit);
    CHECK(i->get_unit_price() == instrument_unit::dollar);
    CHECK(i->get_isin() == "US0378331005");
    CHECK(i->get_ticker() == "AAPL");
    CHECK(i->get_price() == 300.0);

    Instrument * j = Instrument::make_cash_instrument(
        idmanager.ask_id(),
        &c,
        "Dollar",
        instrument_unit::dollar
    );

    CHECK(j->get_type() == instrument_type::cash);
    CHECK(j->get_id() > i->get_id());
    CHECK(i->compare(j) == false);

    Instrument * k = Instrument::make_pending_order_instrument(
        idmanager.ask_id(), 
        i
    );
    CHECK(k->get_pending_order_instrument() == true);
    CHECK(k->compare(i) == true);

}

TEST_CASE("instrument_persistence")
{

    int id_inst = 0;
    constexpr double price = 82.3958;
	{
		Core c("mmcore_test", true);
		auto &idmanager = c.get_id_manager();

		Instrument * i = Instrument::make(
			idmanager.ask_id(),
			&c,
			instrument_type::stock,
			instrument_unit::no_unit,
			instrument_unit::dollar, "AAPL", "US0378331005", "Apple"
		);
        i->set_price(price);

		i->save_in_db();
        id_inst = i->get_id();
        idmanager.update_db();
	}
	{
		// load dataset
		Core c("mmcore_test", false);
        auto &idmanager = c.get_id_manager();

		const auto instrument_ids = c.get_ids(persistence_management::Instrument::db_tablename);
		CHECK(instrument_ids.size() == 1);

        const auto inst_before_loading = c.get_instrument(id_inst);
        CHECK(inst_before_loading == nullptr);

        c.load_deposit();

        const auto inst_after_loading = c.get_instrument(id_inst);
        REQUIRE(inst_after_loading != nullptr);
        CHECK(inst_after_loading->get_price() == price);

        Instrument * j = Instrument::make(
            idmanager.ask_id(),
            &c,
            instrument_type::stock,
            instrument_unit::no_unit,
            instrument_unit::dollar, "CCL", "CA1249002009", "Carnival Corp."
        );
        j->set_price(price/3);
        j->save_in_db();
        CHECK(j->get_id() > id_inst);

	}
}
