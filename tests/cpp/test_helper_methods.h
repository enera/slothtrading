#include "core2/deposit/position.h"
#include "core2/deposit/instrument.h"
#include "core2/deposit/portfolio.h"
#include "core2/core.h"
#include "core2/deposit/instrument_enums.h"
#include "assert.h"

struct test_portfolio
{
    Portfolio * pf;

    Instrument * instr;
    Instrument * cash_instr;

    Position * pos;
    Position * cash_pos;

    const double cash = 500.0;
    const double price = 23.55;


    test_portfolio(Core& c, bool make_default_positions)
    {
        c.make_special_instruments(instrument_unit::dollar);

        auto &idmanager = c.get_id_manager();
        pf = make_default_positions ? 
             Portfolio::make_default_portfolio(&c,"Default Portfolio",  700.0, false) : 
             Portfolio::make(idmanager.ask_id(), &c);

        // make init portfolio with 500 dollar cash and 4 apple stocks

        if (make_default_positions == false)
        {
            // make cash position
            cash_instr = Instrument::make_cash_instrument(
                idmanager.ask_id(),
                &c,
                "dollar",
                instrument_unit::dollar
            );

            cash_pos = Position::make(idmanager.ask_id(), &c, 0.0, cash_instr, pf);
            cash_pos->link();
            cash_pos->link_container();

            pf->set_position_cash(cash_pos);
        }
        else
        {
            // default cash position
            cash_pos = pf->get_position_cash();
        }

        assert(cash_pos != nullptr);

        cash_pos->delta_update(cash);

        // make stock position
        instr = Instrument::make(
            idmanager.ask_id(),
            &c,
            instrument_type::stock,
            instrument_unit::no_unit,
            instrument_unit::dollar, "AAPL", "US0378331005", "Apple"
        );

        instr->set_price(price);

        pos = Position::make(idmanager.ask_id(), &c, 0.0, instr, pf);
        pos->link();
        pos->link_container();
        pos->delta_update(4.0);

    }
};