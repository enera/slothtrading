#include "core2/helper/sqlconnection.h"
#include "core2/helper/idmanager.h"
#include <string>
#include <cppconn/resultset.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("idmanager")
{
    const std::string db_testname = "mmcore_test";
    SQLConnection con = SQLConnection();
    con.connect();
    REQUIRE(con.is_connected());
    con.select_new_db(db_testname);

    {
        IDManager idmanager = IDManager(&con, 0, 0);
        IDManager idmanager2 = IDManager(&con, 1, 15);

        // id manager should create table helper_idmanager
        REQUIRE(con.table_exists("helper_idmanager"));

        int last_id = 0;
        for (int i = 0; i < 100; i++) 
        {
            const int id = idmanager.ask_id();
            CHECK(id > last_id);
            last_id = id;
        }

        CHECK(idmanager.get_current_id() == 100);
        CHECK(idmanager.get_current_id_dbsaved() == 0);

        idmanager.update_db();
        CHECK(idmanager.get_current_id_dbsaved() == 100);

        for (int i = 0; i < 100; i++) 
        {
            const int id = idmanager.ask_id(true);
        }

        CHECK(idmanager.get_current_id() == 200);
        CHECK(idmanager.get_current_id_dbsaved() == 200);

        const int id2 = idmanager2.ask_id(true);
        CHECK(idmanager2.get_current_id() == 16);
        CHECK(idmanager2.get_current_id_dbsaved() == 16);

    }

    // test reconnection of db
    con.disconnect();
    con.connect();
    con.select_db(db_testname);

    IDManager idmanager = IDManager(&con, 0);
    CHECK(idmanager.get_current_id_dbsaved() == 200);

    CHECK(idmanager.ask_id() == 201);
    CHECK(idmanager.get_current_id() == 201);
    con.drop_db(db_testname);
    con.disconnect();
    
}