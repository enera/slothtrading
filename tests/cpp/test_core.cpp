#include "core2/core.h"
#include "core2/deposit/instrument_enums.h"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("core")
{
    Core c("mmcore_test", true);
	c.make_special_instruments(instrument_unit::euro);
	c.reset_special_instruments();
	c.set_special_instruments_from_instrument_map();

}


TEST_CASE("core_persist")
{
	// make save close dataset
	// reload dataset

}