#include "core2/helper/sqlconnection.h"
#include <string>
#include <cppconn/resultset.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("sql connection")
{
    const std::string db_testname = "mmcore_test";
    SQLConnection con = SQLConnection();
    con.connect();
    REQUIRE(con.is_connected());
    con.select_new_db(db_testname);

    auto check_db_exists = [&con](const auto& dbname)
    {
        CHECK(con.get_current_db() == dbname);
        auto res = con.execute_query("SELECT DATABASE()");
        res->next();
        CHECK(res->getString("DATABASE()") == dbname);
    };
    
    check_db_exists(db_testname);

    con.disconnect();
    CHECK(con.is_connected() == false);
    CHECK(con.get_current_db() == "");
    CHECK(con.get_sql_connection() == nullptr);
    
    con.connect();
    con.select_db(db_testname);
    check_db_exists(db_testname);

    const std::vector<std::string> tables = {"a", "simple", "test"};

    for (const auto &t : tables) 
    {
        const auto sqlquery =
          "CREATE TABLE " + t + "(id INT UNSIGNED PRIMARY KEY, data DOUBLE)";
        con.execute_statement(sqlquery);

    }
    for (const auto &t : tables)
        REQUIRE(con.table_exists(t));

    std::vector<std::string> sqlqueries = {
        "INSERT INTO " + tables.at(1) + " (id, data) VALUES (4, 3.2)",
        "INSERT INTO " + tables.at(1) + " (id, data) VALUES (6, 3.7)"
    };
    con.execute_statements(sqlqueries);

    const auto ids = con.get_all_ids_from_table(tables.at(1));
    CHECK(ids.size() == 2);
    CHECK(ids.at(1) == 6);
    
    con.drop_db(db_testname);
    CHECK(con.get_current_db() == "");
    auto res = con.execute_query("SELECT DATABASE()");
    res->next();
    CHECK(res->getString("DATABASE()") == "");

}
