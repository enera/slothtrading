#include "core2/deposit/position.h"
#include "core2/deposit/instrument.h"
#include "core2/deposit/portfolio.h"
#include "core2/deposit/transaction.h"

#include "core2/core.h"
#include "core2/deposit/instrument_enums.h"

#include "test_helper_methods.h"
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("transaction")
{
    Core c("mmcore_test", true);
    auto &idmanager = c.get_id_manager();

    // (1) make init portfolio with 500 dollar cash and 4 apple stocks

    auto test = test_portfolio(c, true);
    test.pf->link();

    constexpr double stocks_to_buy = 2.0;

    const auto pending_cash_pos = test.pf->get_position_pending_orders_cash();

    // buy 2 more apple stocks per market order
    Transaction * t = Transaction::make(
        idmanager.ask_id(),
        &c,
        test.cash_pos,
        test.pos,
        pending_cash_pos,
        stocks_to_buy * test.instr->get_price(),
        stocks_to_buy,
        transaction_sender::strategy,
        booking_target::manual_action,
        booking_ordertype::buy_market,
        0.0,
        0.0
    );

    // test transaction status
    CHECK(t->get_positions_updated() == false);
    CHECK(t->get_status() == booking_status::pending);

    // test positions
    CHECK(t->get_position_from() == test.cash_pos);
    CHECK(t->get_position_to() == test.pos);
    CHECK(t->get_position_pending() == pending_cash_pos);

    // test linking
    t->link();
    t->link_container();
    CHECK(t->get_id_from() == test.cash_pos->get_id());
    CHECK(t->get_id_to() == test.pos->get_id());

    // check if portfolio knows transaction
    auto itr_pos_t = test.pf->get_transactions().find(t->get_id());
    REQUIRE(itr_pos_t != test.pf->get_transactions().end());
    CHECK(itr_pos_t->second == t);

    const auto init_cash = test.cash_pos->get_amount();
    const auto init_cash_pending = pending_cash_pos->get_amount();
    const auto init_stock = test.pos->get_amount();

    // process positions
    t->process();
    const auto updated_cash = test.cash_pos->get_amount();
    const auto updated_cash_pending = pending_cash_pos->get_amount();
    const auto updated_stock = test.pos->get_amount();

    CHECK(updated_cash < init_cash);
    CHECK(updated_cash_pending > 0);
    CHECK(updated_stock == init_stock);

    // update transaction
    // (function has to be called by broker)
    t->update(
        stocks_to_buy * test.instr->get_price() * 0.9, // realized price is lower
        stocks_to_buy, 
        0.0,
        booking_status::fulfilled
    );
    t->process();

    const auto final_cash = test.cash_pos->get_amount();
    const auto final_cash_pending = pending_cash_pos->get_amount();
    const auto final_stock = test.pos->get_amount();

    CHECK(final_cash < init_cash);
    CHECK(final_cash > updated_cash);
    CHECK(final_cash == updated_cash + 0.1 * 2 * test.instr->get_price());
    CHECK(final_cash_pending == init_cash_pending);
    CHECK(final_stock > init_stock );

}


TEST_CASE("transaction_multiple_orders")
{
    // todo: multiple buy and sell orders of different types

    Core c("mmcore_test", true);
    auto &idmanager = c.get_id_manager();

    // (1) make init portfolio with 500 dollar cash and 4 apple stocks

    auto test = test_portfolio(c, true);
    test.pf->link();

    

    const auto pending_cash_pos = test.pf->get_position_pending_orders_cash();

    Transaction * sell_apple = Transaction::make_sell(
        &c,
        test.pf,
        test.instr,
        2,
        booking_target::manual_action,
        booking_ordertype::sell_market
    );

    sell_apple->link();
    sell_apple->link_container();


    // via processing, 
    // pf has to create new pending order position for apple stock

    Transaction * sell_apple_again = Transaction::make_sell(
        &c,
        test.pf,
        test.instr,
        1,
        booking_target::manual_action,
        booking_ordertype::sell_market
    );


    sell_apple_again->link();
    sell_apple_again->link_container();

    sell_apple_again->process();
    sell_apple->process();

    // should have 3 pending apple stocks and 1 remaining as full position

    const auto cash_init = test.pf->get_cash();
    auto apple_pos = test.pf->get_position_by_instrument(test.instr);
    auto apple_pos_pending = test.pf->get_corresponding_pending_orders_position(apple_pos);

    REQUIRE(apple_pos != nullptr);
    REQUIRE(apple_pos_pending != nullptr);

    CHECK(apple_pos->get_amount() == 1);
    CHECK(apple_pos_pending->get_amount() == 3);
    CHECK(test.pf->get_cash() == cash_init);

    // one order was fulfilled, another one was cancelled (simulating the broker)
    sell_apple->update(2.0, 25.02*2, 0.0, booking_status::fulfilled);
    sell_apple_again->update(0.0, 0.0, 0.0, booking_status::cancelled);

    sell_apple_again->process();
    sell_apple->process();

    CHECK(apple_pos->get_amount() == 2);
    CHECK(apple_pos_pending->get_amount() == 0);
    CHECK(test.pf->get_cash() == (cash_init + 25.02*2));


    // make new instrument for ccl
    auto inst_ccl = Instrument::make(
        idmanager.ask_id(), 
        &c, 
        instrument_type::stock,
        instrument_unit::no_unit, 
        instrument_unit::dollar,
        "CCL", 
        "PA1436583006", 
        "Carnival Corporation & plc"
    );

    REQUIRE(c.get_instrument_by_isin("PA1436583006") != nullptr);

}



TEST_CASE("transaction_ensure_ordertypes")
{
    // guarantee that enum ordertype < 100 is buy and > 100 is sell order

    // buy orders
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_closing_auction) == true);
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_limit) == true);
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_market) == true);
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_opening_auction) == true);
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_stop_limit) == true);
    CHECK(Transaction::is_buy_order(booking_ordertype::buy_stop_market) == true);

    // sell orders
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_closing_auction) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_executed_delisting) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_executed_margin_call) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_executed_stop_loss) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_executed_take_profit) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_executed_time_limit) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_limit) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_market) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_opening_auction) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_stop_limit) == false);
    CHECK(Transaction::is_buy_order(booking_ordertype::sell_stop_market) == false);

}
