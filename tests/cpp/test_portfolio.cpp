#include "core2/deposit/position.h"
#include "core2/deposit/instrument.h"
#include "core2/deposit/portfolio.h"
#include "core2/core.h"
#include "core2/deposit/instrument_enums.h"
#include "test_helper_methods.h"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

TEST_CASE("portfolio")
{
    Core c("mmcore_test", true);
    auto &idmanager = c.get_id_manager();

    // (1) make init portfolio with 500 dollar cash and 4 apple stocks

    auto test = test_portfolio(c, false);

    REQUIRE(test.pf->get_positions().size() == 2);
    //test.pf->set_position_cash(test.cash_pos);
    REQUIRE(test.pf->get_cash() == test.cash);
    REQUIRE(test.pf->get_portfolio_balance() == Approx(test.cash + test.price * 4.0));

    REQUIRE(test.pf->get_position_by_instrument(test.cash_instr) == test.cash_pos);
    REQUIRE(test.pf->get_position_by_id(test.pos->get_id()) == test.pos);
    REQUIRE(test.pf->get_positions_by_type(instrument_type::stock).at(0) == test.pos);
    REQUIRE(test.pf->get_positions_by_type(instrument_type::cash).at(0) == test.cash_pos);
    REQUIRE(test.pf->get_position_by_ticker("AAPL") == test.pos);
    REQUIRE(test.pf->get_position_by_isin("US0378331005") == test.pos);

    test.pf->make_special_positions();

    // check if special positions are part of normal positions
    const auto & all_pos = test.pf->get_positions();

	const auto is_part_of_positions = [&all_pos](const auto pos) 
	{ 
		return all_pos.find(pos->get_id()) != all_pos.end(); 
	};

    REQUIRE(is_part_of_positions(test.pf->get_position_cash()));
	REQUIRE(is_part_of_positions(test.pf->get_position_pending_orders_cash()));
	REQUIRE(is_part_of_positions(test.pf->get_position_taxes()));
	REQUIRE(is_part_of_positions(test.pf->get_position_balance_taxes()));
	REQUIRE(is_part_of_positions(test.pf->get_position_balance_margin()));

    // pending order tests ..
    Position * p_pos = test.pf->get_corresponding_pending_orders_position(test.pos);
    REQUIRE(p_pos == nullptr);

    // should create a corresponding pending order position
    p_pos = test.pf->get_corresponding_pending_orders_position(test.pos, true);

    // check for different instruments with same properties
    REQUIRE(p_pos->get_instrument() != test.pos->get_instrument());
    REQUIRE(p_pos->get_instrument()->compare(test.pos->get_instrument()) == true);

    const auto size = test.pf->get_positions().size();
    REQUIRE(p_pos != nullptr);
    REQUIRE(test.pf->get_positions_pending_orders().size() == 2);
    REQUIRE(is_part_of_positions(p_pos));

    // should not add another position
    const auto p_pos2 = test.pf->get_corresponding_pending_orders_position(test.pos, true);
    REQUIRE(test.pf->get_positions().size() == size);
    REQUIRE(p_pos2 == p_pos);


    // (2) make default init portfolio
    auto test2 = test_portfolio(c, true);

    REQUIRE(test2.pf->get_consider_taxes() == false);
    REQUIRE(test2.pf->get_position_cash() != nullptr);
    REQUIRE(test2.pf->get_position_pending_orders_cash() != nullptr);
    test2.pf->get_position_cash()->abs_update(700.0);
    REQUIRE(test2.pf->get_cash() == 700.0);
    
    const auto cash_position = test2.pf->get_position_cash();
    const auto pending_order_position_cash = test2.pf->get_position_pending_orders_cash();

    // check if special positions are part of normal positions
    const auto & all_pos2 = test2.pf->get_positions();
	const auto is_part_of_positions2 = [&all_pos2](const auto pos)
	{
		return all_pos2.find(pos->get_id()) != all_pos2.end();
	};

	REQUIRE(is_part_of_positions2(test2.pf->get_position_cash()));
	REQUIRE(is_part_of_positions2(test2.pf->get_position_pending_orders_cash()));
	REQUIRE(is_part_of_positions2(test2.pf->get_position_taxes()));
	REQUIRE(is_part_of_positions2(test2.pf->get_position_balance_taxes()));
	REQUIRE(is_part_of_positions2(test2.pf->get_position_balance_margin()));

    // should change nothing
    test2.pf->make_special_positions();

    REQUIRE(test2.pf->get_position_cash() == cash_position);
    REQUIRE(test2.pf->get_position_pending_orders_cash() == pending_order_position_cash);

    REQUIRE(test2.pf->get_positions().size() == 6);
    
    test2.pf->link();

    REQUIRE(test2.pf->get_position_balance_margin()->get_id() == test2.pf->get_id_position_balance_margin());
    REQUIRE(test2.pf->get_position_balance_taxes()->get_id() == test2.pf->get_id_position_balance_taxes());


}