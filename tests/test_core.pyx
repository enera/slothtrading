from moneymaker.core.core cimport Core
from moneymaker.core.deposit.instrument_enum cimport *

def test_core_init():
    cdef Core core = Core("mmcore_test")
    assert core.idmanager is not None
    assert core.con is not None
    assert core.con.current_db == "mmcore_test"

    core.make_special_instruments(unit_euro)

    assert core.instrument_cash is not None
    assert core.instrument_cash.type == type_cash
    assert core.instrument_pending_orders is not None
    assert core.instrument_pending_orders.type == type_pending_order
    assert core.instrument_taxes is not None
    assert core.instrument_taxes.type == type_taxes
    assert core.instrument_balance_taxes is not None
    assert core.instrument_balance_taxes.type == type_balance_taxes
    assert core.instrument_balance_margin is not None
    assert core.instrument_balance_margin.type == type_balance_margin

    assert len(core.instruments) == 5
    assert len(core.persistence_objects) == 5

def test_core():
    test_core_init()
