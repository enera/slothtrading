from test_transaction import test_transaction
from test_idmanager import test_idmanager
from test_db_connection import test_db_connection
from test_core import test_core


def run_cython_test(cython_func, name):
    print(f"Run test {name}")
    cython_func()
    print("\tpassed")


def test_db_connection_cython():
    run_cython_test(test_db_connection, "db connection")


def test_transaction_cython():
    run_cython_test(test_transaction, "transaction")


def test_idmanager_cython():
    run_cython_test(test_idmanager, "idmanager")


def test_core_cython():
    run_cython_test(test_core, "core")


if __name__ == "__main__":
    # call the tests
    test_db_connection_cython()
    test_transaction_cython()
    test_idmanager_cython()
    test_core_cython()

