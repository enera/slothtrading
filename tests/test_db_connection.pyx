from moneymaker.core.deposit.transaction cimport *
from moneymaker.core.helper.sqlconnection cimport SQLConnection


def test_db_connection():

    cdef str db_testname = "mmcore_test"

    cdef SQLConnection con = SQLConnection()
    assert con.connection_info is not None
    assert all(
        key in list(con.connection_info.keys())
        for key in ["user", "password", "host", "port"]
    )

    assert con.current_db is None

    con.connect()
    assert con.db_obj is not None
    assert con.cursor_obj is not None

    con.select_new_db(db_testname)

    assert con.current_db == db_testname

    con.cursor_obj.execute("SELECT DATABASE()")
    cdef object res = con.cursor_obj.fetchall()
    assert res[0][0] == db_testname

    con.disconnect()
    assert con.db_obj is None
    assert con.cursor_obj is None
    assert con.current_db is None

    con.connect()
    con.select_db(db_testname)
    assert con.current_db == db_testname

    con.cursor_obj.execute("SELECT DATABASE()")
    res = con.cursor_obj.fetchall()
    assert res[0][0] == db_testname

    cdef list tables = ["a", "simple", "test"]
    cdef str t
    cdef str sqlquery
    for t in tables:
        sqlquery = f"CREATE TABLE {t}(id INT UNSIGNED PRIMARY KEY, data DOUBLE)"
        con.cursor_obj.execute(sqlquery)
        con.db_obj.commit()

    for t in tables:
        assert con.table_exists(t)

    assert not con.table_exists("notable")

    con.drop_db(db_testname)
    assert con.current_db is None

    con.cursor_obj.execute("SELECT DATABASE()")
    res = con.cursor_obj.fetchall()
    assert res[0][0] is None

    con.disconnect()
