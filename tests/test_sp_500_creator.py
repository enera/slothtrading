import datetime
from moneymaker.universe_creators.sp_500_creator import sp_500_creator


def test_get_sp_500_ticker_periods():
    start = datetime.datetime.fromisoformat("1998-03-31")
    end = datetime.datetime.fromisoformat("2021-03-31")

    creator = sp_500_creator(start, end)
    sp_500_ticker_periods = creator.get_history()
    max_days = (
        datetime.date.fromisoformat("2021-03-31")
        - datetime.date.fromisoformat("1998-03-31")
    ).days

    for no_day in range(1, max_days + 1):
        current_date = start + datetime.timedelta(days=no_day)
        current_tickers = []
        for ticker in sp_500_ticker_periods.keys():
            for period in sp_500_ticker_periods[ticker]:
                if current_date >= datetime.datetime.fromisoformat(
                    period[0]
                ) and current_date <= datetime.datetime.fromisoformat(period[1]):
                    current_tickers.append(ticker)
        assert len(set(current_tickers)) == len(current_tickers)
        assert len(current_tickers) >= 500
        assert len(current_tickers) <= 505
