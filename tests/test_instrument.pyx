from moneymaker.core.deposit.instrument cimport *
from moneymaker.core.deposit.position cimport *

cdef str isin = 'US0378331005'
cdef str ticker = "AAPL"
cdef int i_id = 007

def test_instrument_make_isin():
    cdef instrument_type i_type = type_stock
    cdef instrument_unit i_unit = unit_euro
    cdef instrument_unit unit_price = unit_euro

    cdef Instrument i = Instrument.make(
        i_id,
        None,
        i_type,
        i_unit,
        unit_price,
        ticker,
        isin
    )

    assert i.id == 007
    assert i.type == type_stock
    assert i.unit == unit_euro
    assert i.unit_price == unit_euro
    assert i.ticker == 'AAPL'
    assert i.isin == 'US0378331005'

def test_instrument_make_no_isin():
    cdef str isin = ''
    cdef instrument_type i_type = type_stock
    cdef instrument_unit i_unit = unit_euro
    cdef instrument_unit unit_price = unit_euro

    cdef Instrument i = Instrument.make(
        i_id,
        None,
        i_type,
        i_unit,
        unit_price,
        ticker,
        isin
    )

    assert i.id == 007
    assert i.type == type_stock
    assert i.unit == unit_euro
    assert i.unit_price == unit_euro
    assert i.ticker == 'AAPL'
    assert i.isin == ''


def test_instrument_append_position():
    cdef instrument_type i_type = type_stock
    cdef instrument_unit i_unit = unit_euro
    cdef instrument_unit unit_price = unit_euro

    # init instrument
    cdef Instrument i = Instrument.make(
        i_id,
        None,
        i_type,
        i_unit,
        unit_price,
        ticker,
        isin
    )

    assert i.id == 007
    assert i.type == type_stock
    assert i.unit == unit_euro
    assert i.unit_price == unit_euro
    assert i.ticker == 'AAPL'
    assert i.isin == 'US0378331005'

    # init position
    cdef int p_id = 8
    cdef double amount = 500
    cdef Position p = Position.make(
        p_id,
        None,
        amount,
        i,
        None
    )

    assert p.id == 8, "Wrong Position ID"
    assert p.instrument == i

    # append position to instrument
    cdef dict new_positions = dict([("Position_8", p)])

    i.append_positions(new_positions)

    assert len(i.positions) == 1, 'Number of positions in instrument should be 1'
    assert i.positions['Position_8'] == p, 'Wrong Position object in instrument'
