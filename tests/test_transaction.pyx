from moneymaker.core.deposit.transaction cimport *
from moneymaker.core.helper.sqlconnection cimport SQLConnection
from random import randrange
cimport numpy as cnp
import numpy as np

cnp.import_array()

cdef cnp.dtype transaction_data_type = cnp.dtype(
    [
        ("id", np.int_),
        ("id_from", np.int_),
        ("id_to", np.int_),
        ("amount_from", np.float64),
        ("amount_to", np.float64),
        ("amount_from_realized", np.float64),
        ("amount_to_realized", np.float64),
        ("time_send", np.float64),
        ("time_realized", np.float64),
        ("commissions_unit_id", np.int_),
        ("commissions_realized", np.float64),
        ("sender", np.int_),
        ("status", np.int_),
        ("target", np.int_),
        ("ordertype", np.int_),
        ("order_limit_price", np.float64),
        ("use_stop_loss", np.bool_),
        ("stop_price", np.float64),
        ("use_take_profit", np.bool_),
        ("take_profit_price", np.float64),
        ("use_execution_datetime", np.bool_),
        ("execution_datetime", np.float64),
        ("id_connected_transaction", np.int_)
    ]
)

def get_data(t : Transaction) -> np.array:
    return np.array((
        t.id,
        t.id_from,
        t.id_to,
        t.amount_from,
        t.amount_to,
        t.amount_from_realized,
        t.amount_to_realized,
        t.time_send,
        t.time_realized,
        t.commissions_unit_id,
        t.commissions_realized,
        t.status,
        t.sender,
        t.target,
        t.ordertype,
        t.order_limit_price,
        t.use_stop_loss,
        t.stop_price,
        t.use_take_profit,
        t.take_profit_price,
        t.use_execution_datetime,
        t.execution_datetime,
        t.id_connected_transaction
    ), dtype=transaction_data_type)


def make_environment():
    # make idmanager
    # make core
    # make default portfolio
    # add instruments
    # add positions for instruments to portfolio
    # link everything
    pass


def test_transaction_make():
    cdef Transaction t = Transaction.make(
        42,
        None,
        None,
        None,
        17.0,
        34.0,
        sender_strategy,
        target_manual_action,
        ordertype_limit,
        22.0
    )

    assert t.id == 42
    assert t.amount_from == 17.0
    assert t.amount_to == 34.0
    assert t.status == status_pending

    t.add_stop_loss(33.33)

    assert t.use_stop_loss == True
    assert t.stop_price == 33.33

    t.add_take_profit(35.35)

    assert t.use_take_profit == True
    assert t.take_profit_price == 35.35

    t.add_execution_datetime(32698)

    assert t.use_execution_datetime == True
    assert t.execution_datetime == 32698

    t.update(18.0, 35.0, 17.4, status_fulfilled)

    assert t.amount_from_realized == 18.0
    assert t.amount_to_realized == 35.0
    assert t.commissions_realized == 17.4
    assert t.status == status_fulfilled



def test_transaction_make_buy_sell():

    make_environment()
    # test make_buy / make_sell
    pass


def test_transaction_link():
    pass

def test_transaction_process_order():
    pass

def test_transaction_database():

    cdef SQLConnection con = SQLConnection()
    con.connect()
    con.select_new_db("mmcore_test")

    cdef object db = con.db_obj
    cdef object cursor = con.cursor_obj

    cdef int i
    cdef Transaction t

    cdef cnp.ndarray compare_data = cnp.ndarray(shape=(23), dtype=transaction_data_type)


    # make some market orders
    for i in range(1,10):
        t = Transaction.make(
            i,
            None,
            None, 
            None, 
            30.0, 
            3.0,
            sender_strategy,
            target_manual_action, 
            ordertype_market, 
            0.0
        )

        t.id_from = randrange(100)
        t.id_to = randrange(100)
        compare_data[i-1] = get_data(t)
        t.save_in_db(db)

    
    # make some limit orders
    for i in range(10,20):
        t = Transaction.make(
            i, 
            None,
            None, 
            None, 
            10.0, 
            17.0,
            sender_strategy,
            target_manual_action, 
            ordertype_limit, 
            randrange(100)/10
        )
        t.id_from = randrange(100)
        t.id_to = randrange(100)
        compare_data[i-1] = get_data(t)
        t.save_in_db(db)

    # update some orders
    cdef Transaction t_loaded
    for i in range(7,17):
        t_loaded = Transaction.load_from_db(db, i, None)

        assert((get_data(t_loaded) == compare_data[i-1]).all())

        t_loaded.update(200.0,10.4,0.50, status_fulfilled)

        # cancel one order
        if i == 15:
            t_loaded.update(0.0,0.0,0.50, status_cancelled)
            assert(t_loaded.status == status_cancelled)
        else:
            assert(t_loaded.amount_from_realized == 200.0)
            assert(t_loaded.amount_to_realized == 10.4)
            
        assert(t_loaded.time_realized != 0)

        compare_data[i-1] = get_data(t_loaded)

        t_loaded.update_in_db(db)

    # load again all orders and check data
    for i in range(1,20):
        t_loaded = Transaction.load_from_db(db, i, None)
        assert((get_data(t_loaded) == compare_data[i-1]).all())
        if i == 15:
            assert(t_loaded.status == status_cancelled)


    #clean up
    con.drop_db("mmcore_test")
    con.disconnect()



def test_transaction():
    test_transaction_make()
    test_transaction_make_buy_sell()
    test_transaction_link()
    test_transaction_process_order()
    test_transaction_database()