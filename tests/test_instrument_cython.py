from .test_instrument import *


def run_cython_test(cython_func, name):
    print(f"Run test {name}")
    cython_func()
    print("\tpassed")


def test_make_instrument_py_1():
    run_cython_test(test_instrument_make_isin, 'make instrument with isin')


def test_make_instrument_py_2():
    run_cython_test(test_instrument_make_no_isin, 'make instrument without isin')


def test_make_instrument_py_3():
    run_cython_test(test_instrument_append_position, 'make instrument and position, append position')


if __name__ == "__main__":
    # call the tests
    test_make_instrument_py_1()
    test_make_instrument_py_2()
    test_make_instrument_py_3()

