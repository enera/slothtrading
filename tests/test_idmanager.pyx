from moneymaker.core.helper.sqlconnection cimport SQLConnection
from moneymaker.core.helper.idmanager cimport IDManager
import asyncio


async def make_process_to_ask_for_ids(process_id: int, id_manager: IDManager):

    # print(f"process started with id {process_id}")
    cdef int i
    for i in range(200):
        id_recieved = await id_manager.consumer_ask_id()
        # print(f"IDManager_Consumer: Recieved for {process_id} ID {id_recieved}")


async def test_id_manager_async():

    cdef SQLConnection con = SQLConnection()
    con.connect()
    con.select_new_db("mmcore_test")

    #cdef object db = con.db_obj
    #cdef object cursor = con.cursor_obj

    # Create a queue
    q = asyncio.Queue(maxsize=1)
    # init idmanager
    id_manager = IDManager(q, con, 0)

    # run 4 tasks to create each 200 ids
    t1 = asyncio.create_task(make_process_to_ask_for_ids(1, id_manager))
    t2 = asyncio.create_task(make_process_to_ask_for_ids(2, id_manager))
    t3 = asyncio.create_task(make_process_to_ask_for_ids(3, id_manager))
    t4 = asyncio.create_task(make_process_to_ask_for_ids(4, id_manager))

    await t1
    await t2
    await t3
    await t4

    await id_manager.cancel()
    assert(id_manager.current_id == 800)

    # make a second id manager (has to load max id from db)
    q2 = asyncio.Queue(maxsize=1)
    id_manager2 = IDManager(q2, con, 0)

    t5 = asyncio.create_task(make_process_to_ask_for_ids(1, id_manager2))
    t6 = asyncio.create_task(make_process_to_ask_for_ids(2, id_manager2))

    await t5
    await t6
    assert(id_manager2.current_id == 1200)
    con.drop_db("mmcore_test")

def test_id_manager_sync():
    cdef SQLConnection con = SQLConnection()
    con.connect()
    con.select_new_db("mmcore_test")
    id_manager = IDManager(None, con, 0)

    cdef int i
    cdef int max = 100

    for i in range(max):
        # not updated in DB
        id_manager.ask_id_sync()

    assert id_manager.current_id == 100
    assert id_manager.current_id_dbsave == 0

    id_manager.update_in_db()

    assert id_manager.current_id == 100
    assert id_manager.current_id_dbsave == 100

    for i in range(max):
        # auto updated in DB
        id_manager.ask_id_sync(True)

    assert id_manager.current_id == 200
    assert id_manager.current_id_dbsave == 200


def test_idmanager():
    asyncio.run(test_id_manager_async())
    test_id_manager_sync()
